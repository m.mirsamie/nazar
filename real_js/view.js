function openLoginDialog()
{
    //myApp.alert('شما در سیستم لاگین نشده اید ، لطفا وارد شوید',alert_head);
    myApp.loginScreen();
}

function server_ok(stat) {
    if (stat === true)
    {
        if ($(".net_ok").length === 0)
        {
            $(".navbar-inner .right").prepend('<img class="net_ok" src="img/icons/wifi_inline_icon.png" style="height:50%;"/>');
        }

    }
    else
    {
        $(".net_ok").remove();
    }
}
function loginView() {
    var hs_usr = $("#username").val().trim();
    var hs_pass = $("#pass").val().trim();
    $(".err").html('');
    if (hs_usr.length < 3)
    {
        $("#username_err").html('نام کاربری باید بیش از سه حرف باشد');
        return(false);
    }
    if (hs_pass.length < 3)
    {
        $("#pass_err").html(' رمز عبور باید بیش از سه حرف باشد');
        return(false);
    }
    $("#khoon").html('<span class="preloader"></span>');
    loginUser(hs_usr, hs_pass, function () {
        myApp.alert("ورود با موفقیت انجام شد", alert_head);
        myApp.closeModal(".login-screen");
        $(".user").text(user);
        drawFormsDb();
        //net_stat_send_enabled=true;
        //sendStats();
    });
}