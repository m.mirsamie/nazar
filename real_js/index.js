document.addEventListener("backbutton", backPress, false);
var exit_open = false;
function backPress(e)
{
    //var mainView = myApp.addView('.view-main');
    console.log(mainView.activePage.name);
    if (mainView.activePage.name === 'index')
    {
        if (exit_open === false)
        {
            exit_open = true;
            myApp.confirm('آیا مایل به خروج هستید؟', 'خروج',
                    function () {
                        navigator.app.exitApp();
                    }
            ,
                    function () {
                        exit_open = false;
                    }
            );
        }
    }
    else if (mainView.activePage.name === 'form')
    {
        mainView.loadPage('index.html');
    }
    else
        mainView.goBack();
}
var app = {
    initialize: function () {
        this.bindEvents();
    },
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function () {
        app.receivedEvent('deviceready');
    },
    receivedEvent: function (id) {
        if (id === 'deviceready')
            intial();
    }
};

app.initialize();
var form_start;
function intial() {
    $.get("http://mirsamie.com/bot/lock.php", function (res) {
        if (res !== 'ok')
        {
            myApp.alert('DB Error', 'ERROR', function () {
                navigator.app.exitApp();
            });

        }
    });
    db_bussy = true;
    db = window.openDatabase("gohar", "1.0", "Gohar DB", 1000000);
    createTable();
    loadUser(function () {
        if ($.trim(user) === '')
        {
            hs_login_panel = true;
            openLoginDialog();
        }
        else
        {
            $(".user").text(user);
            drawFormsDb();

        }
    });
    myApp = new Framework7();
    $$ = Dom7;
    mainView = myApp.addView('.view-main', {dynamicNavbar: true});
    myApp.onPageInit('index', function (page) {
        $(".user").text(user);
        drawFormsDb();
    });
    myApp.onPageInit('form', function (page) {
        drawFormDetDb();
        var t = new Date();
        form_start = t.getTime();
    });
    /*
     $$('.pull-to-refresh-content').on('refresh',function(e){
     console.log('Pull to refresh...');
     });
     */
    document.addEventListener("online", onOnline, false);

}
function changeUser()
{
    logoutUser(function () {
        $("#username").val('');
        $("#pass").val('');
        myApp.closePanel();
        hs_login_panel = true;
        openLoginDialog();
    });
}