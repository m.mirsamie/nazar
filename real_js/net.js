function backupDB(table)
{
    if (typeof table !== 'undefined' && $.trim(table) !== '') {
        console.log("start_backup");
        ex_sql("select * from " + table + " limit " + startB + "," + kileB, function (data, a) {
            if (data.length > 0)
            {
                $.post(backup_url, {"table": table, "backup": data}, function (result) {
                    results += result + "<br/>";
                    startB += kileB;
                    backupDB();
                });
            }
            else
            {
                startB = 0;
                myApp.alert('done send.');
                console.log(results);
            }
        });
    }
}
function onOnline(e)
{
    connection_type = checkConnection();
    //console.log(connection_type);
    if (connection_type === 'none')
        server_ok(false);
    else
        server_ok(true);
    setTimeout(function () {
        onOnline();
    }, con_per);
}
function checkConnection() {
    var networkState = navigator.network.connection.type;
    var states = {};
    states[Connection.UNKNOWN] = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI] = 'WiFi connection';
    states[Connection.CELL_2G] = 'Cell 2G connection';
    states[Connection.CELL_3G] = 'Cell 3G connection';
    states[Connection.CELL_4G] = 'Cell 4G connection';
    states[Connection.NONE] = 'No network connection';
    return networkState;
}
//----------------------------------------FORM----------------------------------
var frm_tmp = '<div class="card" #st#>\
                    <div class="card-header">#card_header#</div>\
                    <div class="card-content">\
                        <div class="card-content-inner">#card_content#</div>\
                    </div>\
                    <div class="card-footer">#card_footer#</div>\
               </div>';
var form_loading = false;
var forms_data = [];
var forms_insert_index = 0;
var questions_data = [];
var questions_insert_index = 0;
function addForm(fn)
{
    if (forms_insert_index < forms_data.length)
    {
        var items = forms_data[forms_insert_index];
        console.log("select id from forms where meta_serial = '" + items.meta_serial + "'");
        ex_sql("select id from forms where meta_serial = '" + items.meta_serial + "'", function (a, b) {
            console.log(a, b);
            if (a.length === 0)
            {
                var query = 'insert into forms ';
                var feilds = ['en'];
                var values = [1];
                for (var feild in items)
                {
                    if (typeof items[feild] !== 'function')
                    {
                        feilds.push(feild);
                        values.push(items[feild]);
                    }
                }
                query += '(' + feilds.join() + ") values ('" + values.join("','") + "')";
                console.log(query);
                forms_insert_index++;
                ex_sql(query, function (a, b) {
                    addForm(fn);
                });
            }
            else
            {
                console.log(items.meta_serial + ' tekrarist');
                forms_insert_index++;
                addForm(fn);
            }
        });
    }
    else
    {
        forms_data = [];
        forms_insert_index = 0;
        if (typeof fn === 'function')
        {
            fn();
        }
    }
}
function addQuestion(fn)
{
    if (questions_insert_index < questions_data.length)
    {
        var items = questions_data[questions_insert_index];
        console.log("select id from questions where meta_id = '" + items.meta_id + "'");
        ex_sql("select id from questions where meta_id = '" + items.meta_id + "'", function (a, b) {
            if (a.length === 0)
            {
                var query = 'insert into questions ';
                var feilds = [];
                var values = [];
                for (var feild in items)
                {
                    if (typeof items[feild] !== 'function')
                    {
                        feilds.push(feild);
                        values.push(items[feild]);
                    }
                }
                query += '(' + feilds.join() + ") values ('" + values.join("','") + "')";
                console.log(query);
                questions_insert_index++;
                ex_sql(query, function (a, b) {
                    addQuestion(fn);
                });
            }
            else
            {
                questions_insert_index++;
                addQuestion(fn);
                console.log(items.meta_serial + ' ' + items.meta_id + ' tekrarist');
            }
        });
    }
    else
    {
        questions_data = [];
        questions_insert_index = 0;
        if (typeof fn === 'function')
        {
            fn();
        }
    }
}
function loadForms()
{
    if (form_loading === false)
    {
        $(".load-form").addClass("active");
        console.log('load forms run');
        $(".mm-header").html("در حل دریافت پیشنویس ها از سرور");
        form_loading = true;
        $("#form_list").html("<div align='center'><img width='30%' src='img/icons/refresh.GIF' ></div>");
        $.get("https://apex.oracle.com/pls/apex/dadehkavan/ctitle/" + user_id, function (res) {
            form_loading = false;
            $(".load-form").removeClass("active");
            var items = res.items;
            if (items.length === 0)
            {
                myApp.alert('هیچ گونه فرمی برای دریافت وجود ندارد', alert_head);
            }
            /*
             var form_metas = [];
             for(var k = 0;k < items.length;k++)
             {
             form_metas.push(items[k].meta_serial);
             }
             */
            forms_data = items;
            forms_insert_index = 0;
            $.get("https://apex.oracle.com/pls/apex/dadehkavan/mquestion/" + user_id, function (qres) {
                questions_data = qres.items;
                questions_insert_index = 0;
//                ex_sql("delete from forms", function (a, b) {
//                    ex_sql("delete from questions", function (a, b) {
                addForm(function () {
                    console.log('add form done');
                    addQuestion(function () {
                        console.log('add question done');
                        drawFormsDb();
                    });
                });
//                    });
//                });
            }).fail(function () {
                $(".mm-header").html("پیش نویس ها");
                form_loading = false;
                myApp.alert('خطا در اتصال به سرور', alert_head);
                $(".load-form").removeClass("active");
                drawFormsDb();
            });
            /*
             $("#form_list").html('');
             for (var i = 0; i < items.length; i++)
             {
             var hed = ((typeof items[i].persian_start_date !== 'undefined' && typeof items[i].persian_end_date !== 'undefined') ? items[i].persian_start_date + ' - ' + items[i].persian_end_date : '----');
             var frm = frm_tmp.replace(/#card_header#/g, hed).replace(/#card_content#/g, items[i].case_title).replace(/#card_footer#/g, "<a href='#' onclick='showForm(" + items[i].meta_serial + ");'>مشاهده</a>");
             $("#form_list").append(frm);
             }
             */
        }).fail(function () {
            $(".mm-header").html("پیش نویس ها");
            form_loading = false;
            myApp.alert('خطا در اتصال به سرور', alert_head);
            $(".load-form").removeClass("active");
            drawFormsDb();
        });
    }
    else
    {
        console.log('load in progress');
    }
}
var send = {};
var en_time = '';
function sendForm(meta_serial,dobj)
{
    ex_sql("select en_time from forms where meta_serial = " + meta_serial, function (forms, b) {
        if (forms.length === 1)
        {
            en_time = forms[0].en_time;
            ex_sql("select * from questions where meta_serial = " + meta_serial, function (items, b) {
                send['items'] = items;
                send['i'] = 0;
                $(dobj).addClass('disabled');
                $(dobj).before("<div align='center'><img width='20px' src='img/icons/refresh.GIF' ></div>");
                startSendForm(dobj);
            });
        }
    });

}
function startSendForm(dobj)
{
    var items = send.items;
    var i = send.i;
    if (i < items.length)
    {
        var address = '----';
        if (geoData)
        {
            address = geoData.results[0].formatted_address;
        }
        $.ajax({
            url: "https://apex.oracle.com/pls/apex/dadehkavan/metaupdate/" + items[i].meta_id + "/" + items[i].meta_value + "/" + address + "/" + en_time + "/" + lon + "/" + lat,
            type: 'PUT',
            success: function (res) {
                i++;
                send.i = i;
                startSendForm();
            },
            error: function () {
                myApp.alert('خطای دسترسی<br/>اتصال اینترنت و شبکه را بررسی کنید', alert_head);
            }
        });
    }
    else
    {
        $(dobj).removeClass('disabled');
        if (items[i - 1])
        {
            console.log("update forms set en = 2 where meta_serial = '" + items[i - 1].meta_serial + "'");
            ex_sql("update forms set en = 2 where meta_serial = '" + items[i - 1].meta_serial + "'", function (a, b) {
                myApp.alert('ارسال با موفقیت انجام شد', alert_head);
                drawFormsDb();
            });
        }
        else
        {
            myApp.alert('ارسال با موفقیت انجام شد', alert_head);
            drawFormsDb();
        }
    }
}
var draw = {};
function drawFormsDb()
{
    $(".mm-header").html("پیش نویس ها");
    $("#form_list").html("<div align='center'><img width='30%' src='img/icons/refresh.GIF' ></div>");
    ex_sql("select * from forms where en = 1", function (items, b) {
        $("#form_list").html('<div style="font-size:20px;color:red;text-align:center;">' + 'اطلاعاتی جهت نمایش موجود نمی باشد' + '</div>');
        draw['items'] = items;
        draw['i'] = 0;
        if (items.length > 0)
        {
            $("#form_list").html('');
            drawFormDbDet();
        }
        else
        {
            myApp.alert('هیچ گونه فرمی در پیش نویس برای شما وجود ندارد', alert_head);
        }
    });
}
function drawArchFormsDb()
{
    $(".mm-header").html("بایگانی");
    $("#form_list").html("<div align='center'><img width='30%' src='img/icons/refresh.GIF' ></div>");
    ex_sql("select * from forms where en = 2", function (items, b) {
        $("#form_list").html('<div style="font-size:20px;color:red;text-align:center;">' + 'اطلاعاتی جهت نمایش موجود نمی باشد' + '</div>');
        if (items.length > 0)
        {
            $("#form_list").html('');
        }
        else
        {
            myApp.alert('هیچ گونه فرمی در  بایگانی برای شما وجود ندارد', alert_head);
        }
        for (var i = 0; i < items.length; i++)
        {
            var footer = "<a href='#' class='button' onclick='showForm(" + items[i].meta_serial + "," + items[i].st_time + "," + items[i].en + ",\"" + items[i].case_title + "\");'>مشاهده</a>";
            var en_time = parseInt(items[i].en_time / 60, 10);
            if (isNaN(en_time))
            {
                en_time = 0;
            }
            var hed = items[i].persian_start_date + ' - ' + items[i].persian_end_date + '[' + items[i].st_time + '#' + en_time + ' دقیقه]'; //((typeof items[i].persian_start_date!=='undefined' && typeof items[i].persian_end_date!=='undefined')?items[i].persian_start_date+' - '+items[i].persian_end_date:'----');
            var today = new Date();
            today = today.getTime();
            var exday = new Date(items[i].end_date);
            exday = exday.getTime();
            var is_expired = (exday < today);
            if (is_expired === true)
            {
                footer = '';
                var frm = frm_tmp.replace(/#card_header#/g, items[i].case_title).replace(/#card_content#/g, hed).replace(/#card_footer#/g, footer).replace(/#st#/g, "style='background-color:rgb(240, 170, 181);;'");
            }
            else
            {
                var frm = frm_tmp.replace(/#card_header#/g, items[i].case_title).replace(/#card_content#/g, hed).replace(/#card_footer#/g, footer).replace(/#st#/g, "");
            }
            $("#form_list").append(frm);
        }
    });
}

function drawFormDbDet()
{
    var items = draw.items;
    var i = draw.i;
    if (i < items.length)
    {
        //ex_sql("select count(id) c,sum(case(questions.meta_value) when '0' then 0 else 1 end) aa from questions where meta_serial='" + items[i].meta_serial + "'", function (a, b) {
//        console.log("select count(id) c from questions where meta_serial='" + items[i].meta_serial + "' and meta_value is null");
        ex_sql("select count(id) c from questions where meta_serial='" + items[i].meta_serial + "' and meta_value is null", function (a, b) {
            console.log(a, b);

            var form_done = (a[0].c === 0);//(a.length ===1 && a[0].c === 0);
            var hed = items[i].persian_start_date + ' - ' + items[i].persian_end_date + '[' + items[i].st_time + ' دقیقه]'; //((typeof items[i].persian_start_date!=='undefined' && typeof items[i].persian_end_date!=='undefined')?items[i].persian_start_date+' - '+items[i].persian_end_date:'----');
            var footer = "<a href='#' class='button' onclick='showForm(" + items[i].meta_serial + "," + items[i].st_time + "," + items[i].en + ",\"" + items[i].case_title + "\");'>مشاهده</a>";
            if (form_done)
            {
                footer += "<a href='#' class='button button-fill color-green' onclick='sendForm(" + items[i].meta_serial + ",this);'>ارسال</a>";
            }
            var today = new Date();
            today = today.getTime();
            var exday = new Date(items[i].end_date);
            exday = exday.getTime();
            var is_expired = (exday < today);
            if (is_expired === true)
            {
                footer = '';
                var frm = frm_tmp.replace(/#card_header#/g, items[i].case_title).replace(/#card_content#/g, hed).replace(/#card_footer#/g, footer).replace(/#st#/g, "style='background-color:rgb(240, 170, 181);;'");
            }
            else
            {
                var frm = frm_tmp.replace(/#card_header#/g, items[i].case_title).replace(/#card_content#/g, hed).replace(/#card_footer#/g, footer).replace(/#st#/g, "");
            }
            $("#form_list").append(frm);
            i++;
            draw.i = i;
            drawFormDbDet();
        });
    }
}

function drawFormDetDb()
{
    var d = new Date();
    var ptoday = JalaliDate.gregorianToJalali(d.getFullYear(), d.getMonth() + 1, d.getDate());
    $("#form_det").html("<div align='center'><img width='30%' src='img/icons/refresh.GIF' ></div>");
    ex_sql("select * from questions where meta_serial = " + meta_serial + " order by box_id,meta_id", function (bitems, b) {
        console.log(bitems);
        var nitems = {};
        for (var i = 0; i < bitems.length; i++)
        {
            if (typeof nitems[bitems[i].box_id] === 'undefined')
            {
                nitems[bitems[i].box_id] = [];
            }
            nitems[bitems[i].box_id].push(bitems[i]);
        }
        console.log(nitems);
        var last_box_id = 0;
        var out = '';
        var nlength = 0;
        var radif = 1;
        for (bid in nitems)
        {
            var items = nitems[bid];

            out += '<div class="card">';
            out += '<div class="card-header" onclick="toggleContent(this);">' + items[0].box_title + '</div>';
            out += '<div class="card-content content-block" style="display:none;">';

            for (var i = 0; i < items.length; i++)
            {
                var extra = '';
                if (items[i].meta_type === 'checkbox')
                {
                    extra = ' (چند جوابی)';
                }
                out += '<div style="font-size:20px;">' + radif + ' : ' + items[i].meta_title + extra + '</div>';
                if (items[i].meta_type === 'radio')
                {
                    var meta_index = 1;
                    out += '<div class="list-block"><ul>';
                    for (meta_index = 1; meta_index <= items[i].meta_num; meta_index++)
                    {
                        var val = items[i]['meta_text' + meta_index];
                        out += '<li><label class="label-radio item-content">';
                        var checked = ((parseInt(items[i].meta_value, 10) === meta_index) ? 'checked' : '');
                        out += '<input type="radio" ' + checked + ' class="response ' + items[i].meta_type + '" id="q_' + items[i].id + '_' + meta_index + '" name="q_' + items[i].meta_id + '">';
                        out += '<div class="item-inner"><div class="item-title">' + val + '</div></div></label></li>';
                    }
                    out += '</ul></div>';
                }
                else if (items[i].meta_type === 'checkbox')
                {
                    var meta_index = 1;
                    out += '<div class="list-block"><ul>';
                    for (meta_index = 1; meta_index <= items[i].meta_num; meta_index++)
                    {
                        var val = items[i]['meta_text' + meta_index];
                        out += '<li><label class="label-radio item-content">';
                        var checked = ((parseInt(items[i].meta_value, 10) === meta_index) ? 'checked' : '');
                        out += '<input type="checkbox" ' + checked + ' class="response checkbox" id="q_' + items[i].id + '_' + meta_index + '" value="' + meta_index + '">';
                        out += '<div class="item-inner"><div class="item-title">' + val + '</div></div></label></li>';
                    }
                    out += '</ul></div>';
                }
                else if (items[i].meta_type === 'text' || items[i].meta_type === 'number')
                {
                    var val = (items[i].meta_value !== null) ? items[i].meta_value : '';
                    out += '<div class="list-block"><ul><li><div class="item-content"><div class="item-inner"><div class="item-input">';
                    out += '<input type="' + items[i].meta_type + '"  class="response text" placeholder="پاسخ" id="q_' + items[i].id + '" value="' + val + '">';
                    out += '</div></div></div></li></ul></div>';
                }
                else if (items[i].meta_type === 'textarea')
                {
                    var val = (items[i].meta_value !== null) ? items[i].meta_value : '';
                    out += '<div class="list-block"><ul><li><div class="item-content"><div class="item-inner"><div class="item-input">';
                    out += '<textarea  class="response textarea" placeholder="پاسخ" id="q_' + items[i].id + '" >' + val + '</textarea>';
                    out += '</div></div></div></li></ul></div>';
                }
                else if (items[i].meta_type === 'date')
                {
                    var up_year = ptoday[0];
                    var val = (items[i].meta_value !== null) ? items[i].meta_value : '';
                    var val_year = (val.split('-').length === 3) ? val.split('-')[0] : ptoday[0];
                    var val_month = (val.split('-').length === 3) ? val.split('-')[1] : ptoday[1];
                    var val_day = (val.split('-').length === 3) ? val.split('-')[2] : ptoday[2];
                    console.log(val, val_year, val_month, val_day);
                    out += '<div class="list-block"><ul><li><div class="item-content"><div class="item-inner"><div class="item-input">';
                    out += '<div class="row">';
//                    out += '<div class="col-33"><input type="number" style="border:1px solid black;text-align: center;" class="response date q_' + items[i].id + '" placeholder="روز" id="q_' + items[i].id + '_day" value="' + val_day + '"></div>';
//                    out += '<div class="col-33"><input type="number" style="border:1px solid black;text-align: center;" class="response date q_' + items[i].id + '" placeholder="ماه" id="q_' + items[i].id + '_month" value="' + val_month + '"></div>';
//                    out += '<div class="col-33"><input type="number" style="border:1px solid black;text-align: center;" class="response date q_' + items[i].id + '" placeholder="سال" id="q_' + items[i].id + '_year" value="' + val_year + '"></div>';
                    out += '<div class="col-33"><select style="border:1px solid black;text-align: center;" class="response date q_' + items[i].id + '" placeholder="روز" id="q_' + items[i].id + '_day">' + creatOptions(1, 31, val_day) + '</select></div>';
                    out += '<div class="col-33"><select style="border:1px solid black;text-align: center;" class="response date q_' + items[i].id + '" placeholder="ماه" id="q_' + items[i].id + '_month">' + creatOptions(1, 12, val_month) + '</select></div>';
                    out += '<div class="col-33"><select style="border:1px solid black;text-align: center;" class="response date q_' + items[i].id + '" placeholder="سال" id="q_' + items[i].id + '_year">' + creatOptions(1300, up_year, val_year) + '</select></div>';
                    out += '</div>';
                    out += '</div></div></div></li></ul></div>';
                }
                radif++;
            }
            out += '</div></div>';
            nlength++;
        }
        if (nlength > 0 && gen === 1)
        {
            out += '</div><div style="padding : 10px;"><a href="#" class="button button-big button-round button-fill color-green" onclick="saveForm(this);">ذخیره</a></div>';
        }
        var hed = '<div class="content-block" style="text-align:center;font-size:20px;" >' + form_name + '[' + gst_time + 'دقیقه]' + '</div>';
        $("#form_det").html(hed + out);
        addValidation();
        setDateMax();
    });
}
function addValidation()
{
    $("select.date").change(function () {
        var obj = $(this);
        ;
        var classes = obj.prop("className").split(' ');
        var clas = '';
        var num = '0';
        console.log(classes);
        for (var i = 0; i < classes.length; i++)
        {
            var tmp = String(classes[i]).split('_');
            if (tmp[0] === 'q' && tmp.length === 2)
            {
                num = tmp[1];
                clas = classes[i];
            }
        }
        if (clas !== '')
        {
            var date_obj = {};
            $("select." + clas).each(function (id, feild) {
                var oid = $(feild).prop('id').split('_');
                if (oid.length === 3 && num === oid[1])
                {
                    date_obj[oid[2]] = parseInt($(feild).val(), 10);
                }
            });
            console.log(date_obj);
            var mtmp = JalaliDate.jalaliToGregorian(date_obj.year, date_obj.month, date_obj.day);
            var ptmp = JalaliDate.gregorianToJalali(mtmp[0], mtmp[1], mtmp[2]);
            $("#q_" + num + "_year").val(ptmp[0]);
            $("#q_" + num + "_month").val(ptmp[1]);
            $("#q_" + num + "_day").val(ptmp[2]);
        }
    });
}
function creatOptions(start, end, selected)
{
    var out = '';
    for (var i = start; i <= end; i++)
    {
        out += '<option value = "' + i + '"' + ((i === parseInt(selected, 10)) ? ' selected' : '') + '>' + i + '</option>';
    }
    console.log(out);
    return(out);
}
function setDateMax()
{

}
function saveForm(dobj)
{
    var obj = $(dobj);
    if (obj.hasClass('active'))
    {
        console.log('progress');
    }
    else
    {
        var query = '';
        var id = -1;
        var meta_index = 0;
        obj.addClass('active');
        var t = new Date();
        var form_end = t.getTime();
        en_time = parseInt((form_end - form_start) / 1000, 10);
        var checkbox_answers = {};
        var date_answers = {};
        $("input.response,textarea.response,select.response").each(function (id, feild) {
            if ($(feild).hasClass('radio'))
            {
                if ($(feild).prop('checked'))
                {
                    var tmp = $(feild).prop('id').split('_');
                    id = tmp[1];
                    meta_index = tmp[2];
                    query = "update questions set meta_value = '" + meta_index + "' where id = " + id;
                    ex_sql(query);
                }
            }
            else if ($(feild).hasClass('text'))
            {
                if ($(feild).val().trim() !== '')
                {
                    var tmp = $(feild).prop('id').split('_');
                    id = tmp[1];
                    query = "update questions set meta_value = '" + $(feild).val().trim() + "' where id = " + id;
                    ex_sql(query);
                }
            }
            else if ($(feild).hasClass('textarea'))
            {
                if ($(feild).val().trim() !== '')
                {
                    var tmp = $(feild).prop('id').split('_');
                    id = tmp[1];
                    query = "update questions set meta_value = '" + $(feild).val().trim() + "' where id = " + id;
                    console.log(query);
                    ex_sql(query);
                }
            }
            else if ($(feild).hasClass('checkbox'))
            {
                if ($(feild).prop('checked'))
                {
                    var tmp = $(feild).prop('id').split('_');
                    var question_id = tmp[1];
                    if (typeof checkbox_answers[question_id] === 'undefined')
                    {
                        checkbox_answers[question_id] = [];
                    }
                    checkbox_answers[question_id].push($(feild).val().trim());
                }
            }
            else if ($(feild).hasClass('date'))
            {
                var tmp = $(feild).prop('id').split('_');
                var question_id = tmp[1];
                if (typeof date_answers[question_id] === 'undefined')
                {
                    date_answers[question_id] = {};
                }
                date_answers[question_id][tmp[2]] = $(feild).val().trim();
            }
        });
        console.log(checkbox_answers);
        console.log(date_answers);
        for (i in checkbox_answers)
        {
            query = "update questions set meta_value = '" + checkbox_answers[i].join('-') + "' where id = " + i;
            ex_sql(query);
        }
        for (j in date_answers)
        {
            query = "update questions set meta_value = '" + date_answers[j].year + '-' + date_answers[j].month + '-' + date_answers[j].day + "' where id = " + j;
            ex_sql(query);
        }
        query = "update forms set en_time = " + en_time + " where meta_serial = " + meta_serial;
        ex_sql(query);
        obj.removeClass('active');
        myApp.alert('ذخیره سازی فرم با موفقیت انجام شد', alert_head, function () {
            mainView.loadPage('index.html');
        });
    }
}
function showForm(m, st_t, en, fname)
{
    meta_serial = m;
    gst_time = st_t;
    gen = en;
    form_name = fname;
    mainView.loadPage('form.html');
}

function toggleContent(dobj)
{
    var is_visible = $(dobj).next().is(":visible");
    $(".card-content").hide();
    if (is_visible !== true)
    {
        $(dobj).next().show();
    }

}
var lon = 0;
var lat = 0;
var geoOk = false;
var geoData;
function getGeo()
{
    cordova.plugins.diagnostic.isLocationEnabled(function (enabled) {
        if (enabled)
        {
            navigator.geolocation.getCurrentPosition(function (s) {
                lon = s.coords.longitude;
                lat = s.coords.latitude;
                $(".navbar-inner .left").append('<img class="gps_ok" src="img/icons/gps.png" style="height:40px;"/>');
                geoOk = true;
                $.getJSON("http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&sensor=true&language=fa", function (res) {
                    if (res.status === 'OK')
                    {
                        geoData = res;
                        console.log(res);
                        myApp.alert("موقعیت شما : " + geoData.results[0].formatted_address, alert_head);
                    }
                    else
                    {
                        myApp.alert("امکان دریافت موقعیت مکانی شما میسر نشد", alert_head);
                    }
                }).fail(function () {
                    myApp.alert("امکان دریافت موقعیت مکانی شما میسر نشد", alert_head);
                });
                console.log('success', s);
            }, function (e) {
                myApp.alert('خطا در دریافت اطلاعات مکانی', alert_head);
                console.log('error', e);
            });

        }
        else
        {
            myApp.alert('لطفا GPS را فعال نمایید.', alert_head);
        }
        console.log("Location is " + (enabled ? "enabled" : "disabled"));
    }, function (error) {
        myApp.alert('لطفا GPS را فعال نمایید.', alert_head);
        console.error("The following error occurred: " + error);
    });
}