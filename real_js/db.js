function startSql()
{
    sql_stack_index = 0;
    if (sql_stack_index < sql_stack_objs.length)
    {
        if ($.trim(sql_stack_objs[sql_stack_index].sql_stack) !== '')
        {
            ////console.log(sql_stack_objs[sql_stack_index]);
            sql = '';
            ex_sql_det(sql_stack_objs[sql_stack_index].sql_stack, function (dataset, inId, err) {
                var fn = sql_stack_objs[sql_stack_index].sql_callback_stack;
                if (typeof fn === 'function')
                    fn(dataset, inId, err);
                ////console.log(dataset,inId,err);
                sql_stack_objs[sql_stack_index].done = true;
                //sql_stack_index++;
                sql_stack_objs.splice(0, 1);
                startSql();
            });
        }
        else
        {
            //sql_stack_index++;
            //startSql();
            ////console.log('empty query : ',sql_stack_objs[sql_stack_index]);
        }
    }
    else
    {
        //sql_stack_index = 0;
        //startSql();
        ////console.log('DONE SQLS');
    }
}
function createTable1(tx)
{
    for (var i = 0; i < intialQuery.length; i++)
        tx.executeSql(intialQuery[i]);
}
function createTable()
{
    for (var i = 0; i < intialQuery.length; i++)
    {
        //sql_stack.push(intialQuery[i]);
        //sql_callback_stack.push(false);
        ex_sql(intialQuery[i], false);
    }
}
function errorCB(err) {
    //console.log(sql,err);
    myApp.alert("Error processing SQL: " + err.message, alert_head);
    if (typeof ex_sql_callback === 'function')
        ex_sql_callback([], -1, err);
    db_bussy = false;
    return false;
}

function successCB() {
    db_bussy = false;
}

function populateDBIn(tx) {
    if (sql !== '')
    {
        tx.executeSql(sql);
        if (typeof ex_sqlx_callback === 'function')
            ex_sqlx_callback();
    }
}
function populateDBInOut(tx) {
    if (sql !== '')
    {
        tx.executeSql(sql, [], querySuccess, errorCB);
        sql = '';
    }
}
function querySuccess(tx, results) {
    dataset = [];
    inId = -1;
    res = results;
    rowss = res.rows;
    for (var i = 0; i < rowss.length; i++)
    {
        var row = rowss.item(i);
        dataset.push(row);
    }
    try {
        if (res.rowsAffected > 0)
            inId = res['insertId'];
    } catch (e) {
    }
    if (typeof ex_sql_callback === 'function')
        ex_sql_callback(dataset, inId);
}
function ex_sqlx_det(sqlIn, fn)
{
    sql = sqlIn;
    ex_sqlx_callback = fn;
    db.transaction(populateDBIn, errorCB, successCB);
}
function ex_sql_det(sqlIn, fn)
{
    sql = sqlIn;
    ex_sql_callback = fn;
    db.transaction(populateDBInOut, errorCB, successCB);
}
function ex_sql(sqlIn, fn)
{
    //sql_stack.push(sqlIn);
    //sql_callback_stack.push(fn);
    var sql_started = (sql_stack_objs.length > 0);
    sql_stack_objs.push({
        "sql_stack": sqlIn,
        "sql_callback_stack": fn,
        "done": false
    });
    if (!sql_started)
        startSql();
}
//---------------------Conf-------------------------------------
function conf_add(key, value, fn)
{
    ex_sql("select id from conf where key = '" + key + "'", function (data, a) {
        if (data.length > 0)
        {
            ex_sql("update conf set value='" + value + "' where id = " + data[0].id, function (a, b) {
                if (typeof fn === 'function')
                    fn(data[0].id);
            });
        }
        else
        {
            ex_sql("insert into conf (key,value) values ('" + key + "','" + value + "')", function (a, id) {
                if (typeof fn === 'function')
                    fn(id);
            });
        }
    });
}
function conf_get(key, fn)
{

    ex_sql("select * from conf where key = '" + key + "'", function (data, a) {

        if (data.length > 0)
        {
            if (typeof fn === 'function')
                fn(data[0]);
        }
        else
        {
            if (typeof fn === 'function')
                fn({
                    "id": 0,
                    "key": "",
                    "value": ""
                });
        }
    });

}
function conf_remove(key, fn)
{
    ////console.log("delete from conf where key = '"+key+"'");
    ex_sql("delete from conf where key = '" + key + "'", function (b, a) {
        ////console.log('delete done');
        if (typeof fn === 'function')
            fn();
    });

}
//--------------------------------------------------------------
//---------------------User-------------------------------------
function logoutUser(fn)
{
    ex_sql("delete from forms", function (a, b) {
        ex_sql("delete from questions", function (a, b) {
            conf_remove('user', function () {
                conf_remove('pass', function () {
                    user = '';
                    user_id = 0;
                    if (typeof fn === 'function')
                        fn();
                });
            });
        });
    });


}
function loginUser(username, password, fn)
{
    user = username;
    $.get("https://apex.oracle.com/pls/apex/dadehkavan/identity/" + username + "/" + password, function (resd) {
        console.log(resd);
        var res = resd;
        if (typeof res.user_id !== 'undefined')
        {
            user_id = res.user_id;
            conf_add('user', user, function () {
                conf_add('user_id', user_id, function () {
                    if (typeof fn === 'function')
                        fn();
                });
            });
        }
        else
        {
            myApp.alert("نام کاربری یا رمز عبور اشتباه است", alert_head);
        }

    }).fail(function () {
        myApp.alert("نام کاربری یا رمز عبور اشتباه است", alert_head);
    });
    /*
     
     */
}
function loadUser(fn)
{
    conf_get('user', function (obj1) {
        user = obj1.value;
        conf_get('user_id', function (obj2) {
            user_id = obj2.value;
            if (typeof fn === 'function')
                fn();
        });
    });
}
