-- phpMyAdmin SQL Dump
-- version 4.2.9.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 01, 2015 at 11:37 AM
-- Server version: 5.5.39
-- PHP Version: 5.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `search_engine`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads_img`
--

CREATE TABLE IF NOT EXISTS `ads_img` (
`id` int(11) NOT NULL,
  `url` varchar(150) COLLATE utf8_persian_ci NOT NULL,
  `content` varchar(500) COLLATE utf8_persian_ci NOT NULL,
  `is_home` tinyint(1) NOT NULL DEFAULT '1',
  `tartib` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `ads_img`
--

INSERT INTO `ads_img` (`id`, `url`, `content`, `is_home`, `tartib`) VALUES
(8, '/se/assets/images/ck_img/ads1.png', '', 1, 0),
(9, '/se/assets/images/ck_img/ads1.png', '', 1, 0),
(11, '/se/assets/images/ck_img/ads1.png', '', 1, 0),
(12, '/se/assets/images/ck_img/ads1.png', '', 0, 0),
(13, '/se/assets/images/ck_img/ads1.png', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `airline`
--

CREATE TABLE IF NOT EXISTS `airline` (
`id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `logo_url` varchar(50) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `airline`
--

INSERT INTO `airline` (`id`, `name`, `logo_url`) VALUES
(1, 'ARIANA', 'images/img/aseman.png'),
(2, 'ASEMAN', 'images/img/mahan.PNG'),
(3, 'ATA', ''),
(4, 'ATLAS GLOBAL\n', 'images/img/taban.png'),
(5, 'آتا', 'images/img/ata.png'),
(6, 'آسمان', ''),
(7, 'اتا', ''),
(8, 'اترک', ''),
(9, 'ایران ایر تور\n', 'images/img/aseman.png'),
(10, 'ایران ایرتور\n', ''),
(11, 'ايرتور\n', ''),
(12, 'CASPIAN', ''),
(13, 'CORENDON\n', 'images/img/ata.png'),
(14, 'IRAN AIR\r\n', ''),
(15, 'Iran Air Tour', '');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`id` int(11) NOT NULL,
  `iata` varchar(4) COLLATE utf8_persian_ci NOT NULL,
  `name` varchar(200) COLLATE utf8_persian_ci NOT NULL,
  `en_name` varchar(100) COLLATE utf8_persian_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=193 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `iata`, `name`, `en_name`) VALUES
(1, 'MHD', 'مشهد', 'Mashhad'),
(2, 'THR', 'مهر آباد', 'Tehran'),
(3, 'IFN', 'اصفهان', 'Isfahan'),
(4, 'SYZ ', 'شیراز', 'Shiraz'),
(5, 'KIH', 'کیش', 'Kish'),
(6, 'QSM', 'قشم', 'Qeshm'),
(7, 'AWZ', 'اهواز', 'Ahwaz'),
(8, 'RAS', 'رشت', 'Rasht'),
(9, 'ABD', 'آبادان', 'Abadan'),
(10, 'NSH', 'نوشهر', 'Nowshahr'),
(11, 'BND', 'بندرعباس', 'Bandar Abbas'),
(12, 'KER', 'کرمان', 'Kerman'),
(13, 'KSH', 'کرمانشاه', 'Kermanshah'),
(14, 'BUZ', 'بوشهر', 'Bushehr'),
(15, 'TBZ', 'تبریز', 'Tabriz'),
(16, 'NJF', 'نجف', 'Najaf'),
(17, 'JWN', 'زنجان', 'Zanjan'),
(18, 'HMD', 'همدان', 'Hamedan'),
(19, 'DXB', 'دبی', 'Dubai'),
(20, 'OMH', 'ارومیه', 'Urmia'),
(21, 'SRY', 'ساری', 'Sari'),
(22, 'AZD', 'یزد', 'Yazd'),
(23, 'IST', 'استانبول', 'Istanbul'),
(24, 'BGW', 'بغداد', 'Baghdad'),
(25, 'IKA', 'امام خمینی', 'Tehran'),
(26, 'AYT', 'آنتالیا', 'Antalya '),
(27, 'CQD', 'شهرکرد', 'Shahrekord'),
(28, 'DEF', 'دزفول', 'Dezful'),
(29, 'ADU', 'اردبیل', 'Ardebil'),
(30, 'BKK', 'بانکوک', 'Bangkok'),
(31, 'ZBR', 'چابهار', 'Chabahar'),
(32, 'ACZ', 'زابل', 'Zabol'),
(33, 'ANK', 'آنکارا-اتیمسگوت', 'Ankara'),
(34, 'GBT', 'گرگان', 'Gorgan'),
(35, 'RZR', 'رامسر', 'Ramsar'),
(36, 'XBJ', 'بیرجند', 'Birjand'),
(37, 'SAW', 'سبیها', 'Istanbul'),
(38, 'KHD', 'خرم آباد', 'Khorramabad'),
(39, 'AJK', 'اراک', 'Arak'),
(40, 'IHR', 'ایران شهر', 'Iranshahr'),
(41, 'IIL', 'ایلام', 'Ilam'),
(42, 'BJB', 'بجنورد', 'Bojnord'),
(43, 'BXR', 'بم', 'Bam'),
(44, 'BDH', 'بندر لنگه', 'Bandar Lengeh'),
(45, 'MRX', 'بندر ماهشهر', 'Bandar-e Mahshahr'),
(46, 'IAQ', 'بهرگان', 'Bahregan'),
(47, 'PFQ', 'پارس آباد', 'Parsabad'),
(48, 'TCX', 'طبس', 'Tabas'),
(49, 'TEW', 'توحید', 'Tohid'),
(50, 'KHK', 'خارک', 'Khark'),
(51, 'SXI', 'سیری', 'Siri'),
(52, 'JAR', 'جهرم', 'Jahrom'),
(53, 'JYR', 'جیرفت', 'Jiroft'),
(55, 'KHY', 'خوی', 'Khoy'),
(56, 'RJN', 'رفسنجان', 'Rafsanjan'),
(57, 'AFZ', 'سبزوار', 'Sabzevar'),
(58, 'SDG', 'سنندج', 'Sanandaj'),
(59, 'ACP', 'سهند', 'Sahand'),
(60, 'SYJ', 'سیرجان', 'Sirjan'),
(62, 'YEH', 'عسلویه', 'Asaluyeh'),
(63, 'FAZ', 'فسا', 'Fasa'),
(64, 'GCH', 'گچساران', 'Gachsaran'),
(65, 'LRR', 'لار', 'Lar'),
(66, 'LFM', 'لامرد', 'Lamerd'),
(67, 'LVP', 'لاوان', 'Lavan'),
(68, 'YES', 'یاسوج', 'Yasuj'),
(69, 'ESB', 'آنکارا-اسمبو', 'Ankara'),
(70, 'KBL', 'کابل', 'Kabul'),
(71, 'BSR', 'بصره', 'Basra'),
(72, 'KWI', 'کویت', 'Kuwait'),
(73, 'MZR', 'مزار شریف', 'Mazar-i-Sharif'),
(74, 'KDH', 'قندهار', 'Kandahar'),
(75, 'HEA', 'هرات', 'Herat'),
(76, 'BJV', 'بدروم', 'Bodrum'),
(77, 'ISE', 'اسپارتا', 'Isparta'),
(78, 'KUL', 'کوالالامپور', 'Kuala Lumpur'),
(79, 'QUM', 'قم', 'Qom'),
(80, 'DSH', 'دوشنبه', 'Doshanbe'),
(81, 'PVG', 'شانگهای', 'Shanghai'),
(82, 'PEK', 'پکن', 'Beijing'),
(83, 'BEY', 'بیروت', 'Beirut'),
(84, 'CAN', 'گوانگجو', ''),
(85, 'EVN', 'ایروان', 'Yerevan'),
(86, 'FRU', 'بیشکک', 'Bishkek'),
(87, 'EBL', 'اربیل', 'Erbil'),
(88, 'DMM ', 'دمام', 'Dammam'),
(89, 'ECN', 'ارجان', 'Arjan'),
(90, 'CMB', 'کلمبو', 'Colombo'),
(91, 'ISU ', 'سلیمانیه', 'Sulaymaniyah'),
(92, 'DOH ', 'دوحه', 'Doha'),
(93, 'TBS ', 'تفلیس', 'Tbilisi'),
(94, 'SHJ', 'شارجه', 'Sharjah'),
(95, 'MNL ', 'مانیل ', 'Manila'),
(96, 'AMS ', 'آمستردام ', 'Amsterdam'),
(97, 'FRA ', 'فرانکفورت ', 'Frankfurt'),
(98, 'CGN ', 'کلن ', 'Cologne'),
(99, 'BER ', 'برلین ', 'Berlin'),
(100, 'CPH ', 'کپنهاگ ', 'Copenhagen'),
(101, 'HAM', 'هامبورگ ', 'Hamburg'),
(102, 'PAR ', 'پاریس ', 'Paris'),
(103, 'LHR ', 'لندن ', 'London'),
(104, 'GOT ', 'گوتنبرگ ', 'Gutenberg'),
(105, 'ARN ', 'استکهلم ', 'Stockholm'),
(106, 'DPS ', 'بالی ', 'Bali'),
(107, 'CGK ', 'جاکارتا ', 'Jakarta'),
(108, 'DEL', 'دهلی ', 'Delhi'),
(109, 'BOM ', 'بمبئی ', 'Mumbai'),
(110, 'SIN ', 'سنگاپور ', 'Singapore'),
(111, 'MOW ', 'مسکو ', 'Moscow'),
(112, 'JED ', 'جده ', 'Jeddah'),
(113, 'MED ', 'مدینه ', 'Medina'),
(114, 'YUL ', 'منترال ', 'Montréal'),
(115, 'YYZ ', 'تورنتو ', 'Toronto'),
(116, 'IZM ', 'ازمیر ', 'İzmir'),
(117, 'DLM ', 'دالامان ', 'Dalaman'),
(118, 'ADA ', 'آدنا ', 'Odenas'),
(119, 'BBL', 'بابلسر ', 'Babolsar'),
(120, 'GVT ', 'گرگان ', 'Gorgan'),
(121, 'GSM', 'قشم ', 'Qeshm'),
(123, 'AHZ ', 'اهواز ', 'Ahwaz'),
(124, 'ZAH', 'زاهدان ', 'Zahedan'),
(125, 'KMH ', 'كرمنشاه ', 'Kermanshah'),
(127, 'TRZ ', 'تبريز ', 'Tabriz'),
(128, 'BRS ', 'بندر عباس', 'Bandar Abbad'),
(129, 'HMN', 'همدان ', 'Hamedan'),
(130, 'BSM', 'محمودآباد ', 'Mahmudabad'),
(131, 'CBR ', 'چابهار ', 'Chabahar'),
(132, 'CHS ', 'چالوس ', 'Chalus'),
(134, 'gk ', 'گنبدکاووس', 'Gonbad-e Kavus'),
(135, 'MIL ', 'میلان ', 'Milan'),
(136, 'ROM', 'رم ', 'Rome'),
(137, 'TRN ', 'تورین ', 'Turin'),
(138, 'VCE', 'ونیز ', 'Venice'),
(139, 'HAJ ', 'هانوفر ', 'Hanover'),
(140, 'MUC ', 'مونیخ ', 'Munich'),
(141, 'NUE', 'نورمبرگ ', 'Nuremberg'),
(142, 'LYS ', 'لیون ', 'Lyon'),
(143, 'NCE ', 'نیس ', 'Nice'),
(144, 'STR ', 'اشتوتگارد ', 'Stuttgart'),
(145, 'BRU ', 'بروکسل ', 'Brussels'),
(146, 'MSQ ', 'مینسک ', 'Minsk'),
(147, 'SJJ', 'سارایبو ', 'Sarajevo'),
(149, 'ATH ', 'آتن', 'Athens'),
(150, 'BUD ', 'بوداپست ', 'Budapest'),
(151, 'DUB ', 'دوبلین ', 'Dublin'),
(152, 'OSM ', 'موصل ', 'Mosul'),
(153, 'JFK ', 'نیویورک ', 'New York'),
(154, 'IAH ', 'هستون ', 'Hesston'),
(155, 'IAD ', 'واشنگتون ', 'Washington'),
(156, 'GYY ', 'شیکاگو ', 'Chicago'),
(157, 'GZT ', 'غاضی عنتب', ''),
(158, 'TIA ', 'تیرانا ', 'Tirana'),
(159, 'VIE ', 'وین ', 'Vienna'),
(160, 'SOF ', 'صوفیا ', 'Sofia'),
(161, 'PRG ', 'پراگ ', 'Prague'),
(162, 'HEL ', 'هلسینکی ', 'Helsinki'),
(163, 'SPA', 'اسپارتا', 'Isparta'),
(164, 'VAR', 'وارنا', 'Varna'),
(165, 'MLE ', 'ماله', 'Malé'),
(166, 'CPT ', 'کیپ تاون', 'Cape Town'),
(167, 'JNB ', 'ژوهانسبورگ', 'Johannesburg'),
(168, 'AKW ', 'آقاجاری ', 'Aghajari '),
(169, 'kus ', 'کوش آداسی', 'Kuşadası'),
(170, 'msr ', 'ماهشهر ', 'Mahshahr'),
(171, 'bmo ', 'بامبو', 'Bamboo'),
(172, 'ZAG ', 'زاگرب', 'Zagreb'),
(173, 'TLS ', 'تولوس ', 'Tullos'),
(174, 'TLS ', 'تولوس ', 'Tullos'),
(175, 'SKG ', 'تزالونیک', 'Thessaloniki'),
(176, 'BLQ', 'بولونیا ', 'Bologna'),
(177, 'GOA ', 'ژنوآ ', 'Geneva'),
(178, 'LIS ', 'لیسبون ', 'Lisbon'),
(179, 'WAW ', 'ورشو ', 'Warsaw'),
(180, 'TGD ', 'پودروگیکا', ''),
(181, 'KIV ', 'چیسینا ', ''),
(182, 'SKP ', 'اسکوپیه ', 'Skopje'),
(183, 'RIX ', 'ریگا', 'Riga'),
(184, 'NAP ', 'ناپل ', 'Naples'),
(185, 'KYA ', 'کنیا ', 'Kenya'),
(186, 'MEL ', 'ملبورن', 'Melbourne'),
(187, 'PER ', 'پرت ', 'Perth'),
(188, 'SYD ', 'سیدنی ', 'Sydney'),
(190, 'PGU', 'عسلویه', 'Asaluyeh'),
(192, 'BGH', 'بغداد', 'Baghdad');

-- --------------------------------------------------------

--
-- Table structure for table `conf`
--

CREATE TABLE IF NOT EXISTS `conf` (
`id` int(11) NOT NULL,
  `key` varchar(20) COLLATE utf8_persian_ci NOT NULL,
  `value` mediumtext COLLATE utf8_persian_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `conf`
--

INSERT INTO `conf` (`id`, `key`, `value`) VALUES
(1, 'app', 'SE'),
(2, 'title', 'Search Engine'),
(3, 'sender', 'info@tctcenter.ir'),
(4, 'sender_name', 'SE'),
(5, 'changepass_subject', 'تغییر رمز عبور'),
(6, 'changepass_body', 'رمز عبور شما<br/>\r\n#pass#><br/>\r\nمی باشد'),
(7, 'hasPaper', 'TRUE'),
(8, 'home_page', 'home'),
(9, 'local_host', 'localhost'),
(10, 'local_user', 'root'),
(11, 'local_pass', '3068145'),
(12, 'local_db', 'search_engine');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
`id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`) VALUES
(1, 'admin', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
`id` int(11) NOT NULL,
  `content` text COLLATE utf8_persian_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_persian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `content`, `title`) VALUES
(1, '<p><img alt="" src="/se/assets/images/ck_img/01 (1).jpg" style="height:65px; width:50px" /><img alt="" src="/se/assets/images/ck_img/bug.png" style="height:28px; width:50px" />​​ سلام سلام قشنگی<img alt="" src="/se/assets/images/ck_img/icar_logo.jpg" style="float:left; height:129px; line-height:20.7999992370605px; width:200px" title="salam" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt="" src="/se/assets/images/ck_img/fibo.png" style="height:281px; line-height:20.7999992370605px; width:500px" /></p>\r\n', 'bakhshname'),
(2, '<p><img alt="" src="/se/assets/images/ck_img/bug.png" style="height:281px; width:500px" />kjnknkknkn</p>\r\n', 'history'),
(3, '<p>ionnbonin</p>\r\n', 'job');

-- --------------------------------------------------------

--
-- Table structure for table `reserve`
--

CREATE TABLE IF NOT EXISTS `reserve` (
`id` int(11) NOT NULL,
  `refrence_id` int(11) NOT NULL,
  `total_asli` int(11) NOT NULL,
  `total_moshtari` int(11) NOT NULL,
  `bank_result` varchar(300) COLLATE utf8_persian_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `en` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `reserve`
--

INSERT INTO `reserve` (`id`, `refrence_id`, `total_asli`, `total_moshtari`, `bank_result`, `user_id`, `en`) VALUES
(1, 72, 20000000, 10000111, '', -1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rookeshi`
--

CREATE TABLE IF NOT EXISTS `rookeshi` (
`id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `rookeshi` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `rookeshi`
--

INSERT INTO `rookeshi` (`id`, `source_id`, `rookeshi`) VALUES
(1, 1, 111);

-- --------------------------------------------------------

--
-- Table structure for table `tour_img`
--

CREATE TABLE IF NOT EXISTS `tour_img` (
`id` int(11) NOT NULL,
  `tourtitle` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `tourcontent` mediumtext COLLATE utf8_persian_ci NOT NULL,
  `url` varchar(500) COLLATE utf8_persian_ci NOT NULL,
  `tartib` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `tour_img`
--

INSERT INTO `tour_img` (`id`, `tourtitle`, `tourcontent`, `url`, `tartib`) VALUES
(10, 'تور خنک', 'این تور بسیار  خنک است.', '/se/assets/images/ck_img/img1.png', 0),
(11, 'تور مزخرف', 'این تور بسیار مزخرف است.', '/se/assets/images/ck_img/img1.png', 0),
(12, 'تور مفت', 'این تور مفتش هم مفت است.\r\n', '/se/assets/images/ck_img/img1.png', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads_img`
--
ALTER TABLE `ads_img`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `airline`
--
ALTER TABLE `airline`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conf`
--
ALTER TABLE `conf`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reserve`
--
ALTER TABLE `reserve`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rookeshi`
--
ALTER TABLE `rookeshi`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_img`
--
ALTER TABLE `tour_img`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads_img`
--
ALTER TABLE `ads_img`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `airline`
--
ALTER TABLE `airline`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=193;
--
-- AUTO_INCREMENT for table `conf`
--
ALTER TABLE `conf`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `reserve`
--
ALTER TABLE `reserve`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `rookeshi`
--
ALTER TABLE `rookeshi`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tour_img`
--
ALTER TABLE `tour_img`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
