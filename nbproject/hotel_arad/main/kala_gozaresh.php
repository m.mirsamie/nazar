<?php	session_start();
	include_once("../kernel.php");
	if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        if(!$se->can_view || !$conf->anbar)
                die(lang_fa_class::access_deny);
	function loadKala()
	{
		$out = null;
		mysql_class::ex_sql('select `name`,`id` from `kala_no` order by `name`',$q);
		while($r = mysql_fetch_array($q))
		{
			$out[$r['name']] = (int)$r['id'];
		}
		return($out);
	}
	function loadVahed()
	{
		$out = null;
		mysql_class::ex_sql('select `name`,`id` from `kala_vahed` order by `name`',$q);
		while($r = mysql_fetch_array($q))
		{
			$out[$r['name']] = (int)$r['id'];
		}
		return($out);
	}
	function loadMojoodi($inp)
	{
		$out=anbar_det_class::getMojoodi($inp);
		if($out['out']<=0)
			$out = $out['msg'];
		else
			$out = $out['out'];
		return $out;
	}
	function hpdate($inp)
	{
		return(audit_class::hamed_pdate($inp));
	}
	function loadUser($inp)
	{
		$out = new user_class($inp);
		$out = $out->fname.' '.$out->lname;
		return($out);
	}
	function loadTyp($inp)
	{
		$out = new anbar_typ_class($inp);
		$out = $out->name;
		return($out);
	}
	function loadGhimatVahed($id)
	{
		$ad = new anbar_det_class((int)$id);
		$out = ($ad->tedad > 0) ? ceil($ad->ghimat/$ad->tedad) : (0);
		return($out);
	}
	$kala_id = (isset($_REQUEST['kala_id']))?(int)$_REQUEST['kala_id']:-1;
	$kala = new kala_class($kala_id);
	$grid = new jshowGrid_new("anbar_det","grid1");
	$grid->whereClause=" `kala_id`=$kala_id ";
	$grid->width = '95%';
	$grid->index_width = '20px';
	$grid->columnHeaders[0] = null;
        $grid->columnHeaders[1] = null;
	$grid->columnHeaders[2] = "تاریخ";
        $grid->columnFunctions[2] = "hpdate";
	$grid->columnHeaders[3] = null;
	$grid->columnHeaders[4] = "تعداد";
	$grid->columnHeaders[5] = 'قیمت کل';
	$grid->columnHeaders[6] = 'تحویل دهنده';
	$grid->columnFunctions[6] = 'loadUser';
	$grid->columnHeaders[7] = null;
	$grid->columnHeaders[8] = "ورودی/خروجی";
	$grid->columnFunctions[8] ='loadtyp';
	$grid->columnHeaders[9] = null;
	$grid->columnHeaders[10] = null;
	$grid->columnHeaders[11] = null;
	$grid->addFeild('id',5);
	$grid->columnHeaders[5] = 'قیمت واحد';
	$grid->columnFunctions[5] = 'loadGhimatVahed';
	$grid->canEdit = FALSE;
	$grid->canDelete = FALSE;
	$grid->canAdd = FALSE;
        $grid->intial();
   	$grid->executeQuery();
        $out = $grid->getGrid();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->
		<link type="text/css" href="../js/jquery/themes/trontastic/jquery-ui.css" rel="stylesheet" />
		<link type="text/css" href="../js/jquery/window/css/jquery.window.css" rel="stylesheet" />

		<link type="text/css" href="../css/style.css" rel="stylesheet" />

		<!-- JavaScript Includes -->
		<script type="text/javascript" src="../js/jquery/jquery.js"></script>

		<script type="text/javascript" src="../js/jquery/jquery-ui.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>
		</title>
	</head>
	<body>
		<?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>
		<div align="center">
			<br/>
			نام کالا:
				<b><?php echo $kala->name; ?></b>
			<br/>
			<?php	echo $out;?>
		</div>
	</body>
</html>
