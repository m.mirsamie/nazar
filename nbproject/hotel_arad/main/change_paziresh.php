<?php
	session_start();
	include_once('../kernel.php');
	if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        if(!$se->can_view)
                die(lang_fa_class::access_deny);
	$is_admin = $se->detailAuth('all'); 
	$h_id = ((isset($_REQUEST['h_id']))?(int)$_REQUEST['h_id']:-1);
	$room_name1 = ((isset($_REQUEST['room_1']))?$_REQUEST['room_1']:-1);
	$room_name2 = ((isset($_REQUEST['room_2']))?$_REQUEST['room_2']:-1);	
	$r1 = ((isset($_REQUEST['r1']))?(int)$_REQUEST['r1']:'');
	$combo = "";
	$combo .= "<form name=\"selRoom\" id=\"selRoom\" method=\"GET\">";
	$combo .= "اتاق اول : <select class='inp' id=\"room_1\" name=\"room_1\"  style=\"width:auto;\">\n";
	mysql_class::ex_sql("select `id`,`name` from `room` where (`vaziat` = 2 || `vaziat` = 0) and  `en`='1' order by `name`",$q);
        while($r = mysql_fetch_array($q))
        {
		if((int)$r["id"]== (int)$room_name1)
                {
                        $select = "selected='selected'";
                }
                else
                {
                        $select = "";
                }
                $combo .= "<option value=\"".(int)$r["id"]."\" $select   >\n";
                $combo .= $r["name"]."\n";
                $combo .= "</option>\n";
        }
        $combo .="</select>";
	$combo .= "اتاق دوم : <select class='inp' id=\"room_2\" name=\"room_2\"  style=\"width:auto;\">\n";
	mysql_class::ex_sql("select `id`,`name` from `room` where (`vaziat` = 2 || `vaziat` = 0) and `en`='1' order by `name`",$q);
        while($r = mysql_fetch_array($q))
        {
		if((int)$r["id"]== (int)$room_name2)
                {
                        $select = "selected='selected'";
                }
                else
                {
                        $select = "";
                }
                $combo .= "<option value=\"".(int)$r["id"]."\" $select   >\n";
                $combo .= $r["name"]."\n";
                $combo .= "</option>\n";
        }
        $combo .="</select>";
	//$combo .= "شماره رزرو:";
	$combo .= "<input type='text' id='r1' name='r1' style='display:none;' value='0'/>";
	$combo .= "<input id='sub' type='button' value='جابجایی' onclick=\"document.getElementById('selRoom').submit();\" class='inp'/>";
	$combo .= "</form>";
	$room_id1 = -1;
	$room_id2 = -1;
	$jday = date("Y-m-d");
	$msg = '';
	$room_id1 = isset($_REQUEST['room_1'])?(int)$_REQUEST['room_1']:-1;
	$room_id2 = isset($_REQUEST['room_2'])?(int)$_REQUEST['room_2']:-1;
	if($room_id1>0)
	{
		$done = room_det_class::changeDate($room_id1,$room_id2,$jday,$r1);
		if($done)
			$msg = "alert('جابجایی با موفقیت انجام پذیرفت');opener.location=opener.location;";
		else
			$msg = "alert('جابجایی امکان پذیر نیست');";
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link type="text/css" href="../css/style.css" rel="stylesheet" />

		<link type="text/css" href="../js/styles/jquery-ui-1.8.14.css" rel="stylesheet" />
		<script type="text/javascript" src="../js/jquery/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../js/jquery/jquery.ui.datepicker-cc.all.min.js"></script>
		<script type="text/javascript" src="../js/tavanir.js"></script>
		<script type="text/javascript">
			function addReserve(rid,room_id)
			{
				if(document.getElementById('r1').value == '' && document.getElementById('r2').value!=rid)
				{
					document.getElementById('r1').value = rid;
					document.getElementById('room_id1').value = room_id;
					document.getElementById('d1').style.display = '';
				}
				else if(document.getElementById('r2').value == '' && document.getElementById('r1').value!=rid)
				{
					document.getElementById('r2').value = rid;
                                        document.getElementById('room_id2').value = room_id;
					document.getElementById('d2').style.display = '';
				}
				if(parseInt(document.getElementById('room_id1').value,10)>0 && parseInt(document.getElementById('room_id2').value,10)>0)
					return(true);
				else
					return(false);
			}
			function select_reserve(rid,room_id)
			{
				if(addReserve(rid,room_id))
				{
					document.getElementById('loading').style.display='';
					document.getElementById('frm1').submit();
				}
			}
			function unchange()
			{
				document.getElementById('r1').value = '';
				document.getElementById('r2').value = '';
                                document.getElementById('room_id1').value = '';
                                document.getElementById('room_id2').value = '';
				document.getElementById('d1').style.display = 'none';
				document.getElementById('d2').style.display = 'none';
				document.getElementById('mod').value = 'select';
				if(document.getElementById('sub'))
					document.getElementById('sub').style.display='none';
                                if(document.getElementById('toz'))
                                        document.getElementById('toz').style.display='none';
			}
			function change()
			{
				//document.getElementById('mod').value = 'change';
				document.getElementById('frm1').submit();
			}
		</script>
		<script type="text/javascript">
	    $(function() {
	        //-----------------------------------
	        // انتخاب با کلیک بر روی عکس
	        $("#datepicker6").datepicker({
	            showOn: 'button',
		    dateFormat: 'yy/mm/dd',
	            buttonImage: '../js/styles/images/calendar.png',
	            buttonImageOnly: true
	        });
	    });
    </script>
		<style>
			td{text-align:center;}
		</style>
		<title>
			سامانه رزرواسیون	
		</title>
	</head>
	<body>
                <?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>
		<div align="center" style="margin:10%;background-color:#ffffff;-webkit-border-radius: 8px; -moz-border-radius: 8px; border-radius: 8px; border:1px double black; height:40%;">
			<?php
                                echo $combo ;
                        ?>
		</div>
		<script language="javascript">
			<?php echo $msg; ?>
		</script>
	</body>
</html>
