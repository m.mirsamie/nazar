<?php
error_reporting(E_ALL);
ini_set('display_errors', 0);
session_start();
include_once("../kernel.php");
if (!isset($_SESSION['user_id']))
    die(lang_fa_class::access_deny);
$se = security_class::auth((int) $_SESSION['user_id']);
if (!$se->can_view)
    die(lang_fa_class::access_deny);
$is_admin = FALSE;
if ($se->detailAuth('all'))
    $is_admin = TRUE;
$GLOBALS['msg'] = '';
$GLOBALS['sum'] = 0;

function loadMoney($inp) {
    $GLOBALS['sum']+=$inp;
    return(monize($inp));
}

$wer = '';
if (!$is_admin && $se->detailAuth('middle_manager'))
    $wer = '';
else if (!$is_admin && !$se->detailAuth('middle_manager'))
    $wer = ' and `id` = ' . (int) $_SESSION['user_id'];
else
    $wer = '';
$limit = 9999999999999;
$sdate = (isset($_REQUEST['sdate'])) ? hamed_pdateBack2($_REQUEST['sdate']) : date("Y-m-d");
$edate = (isset($_REQUEST['edate'])) ? hamed_pdateBack2($_REQUEST['edate']) : date("Y-m-d");
$my = new mysql_class();
$tsum = 0;
$my->ex_sql("select sum(m_hotel) sm from hotel_reserve where reserve_id > 0 and  m_hotel < $limit and regdat>='$sdate' and regdat<='$edate'", $q);
if ($r = mysql_fetch_assoc($q)) {
    $tsum = (int) $r['sm'];
}

$grid = new jshowGrid_new("hotel_reserve", "grid1");
$grid->width = '99%';
$grid->index_width = '20px';
$grid->pageCount = 20;
$grid->whereClause = " reserve_id > 0 and  m_hotel < $limit and regdat>='$sdate' and regdat<='$edate'";
$grid->columnHeaders[0] = '';
$grid->columnHeaders[1] = 'نام';
$grid->columnHeaders[2] = 'نام خانوادگی';
$grid->columnHeaders[3] = '';
$grid->columnHeaders[4] = 'شماره رزرو';
$grid->columnHeaders[5] = '';
$grid->columnHeaders[6] = '';
$grid->columnHeaders[7] = '';
$grid->columnHeaders[8] = '';
$grid->columnHeaders[9] = 'مبلغ';
$grid->columnFunctions[9] = 'loadMoney';
$grid->columnHeaders[10] = '';
$grid->columnHeaders[11] = '';
$grid->columnHeaders[12] = '';
$grid->columnHeaders[13] = '';
$grid->columnHeaders[14] = '';
$grid->columnHeaders[15] = '';
$grid->columnHeaders[16] = '';
$grid->canAdd = FALSE;
$grid->canDelete = FALSE;
$grid->canEdit = FALSE;
$grid->intial();
$grid->executeQuery();
$grid->footer = "<tr class='showgrid_insert_row'  ><td style='text-align:left;' colspan='5'> جمع  : <span id='jam'>۰</span>ریال جمع کل :<span id='tsum'>۰</span> ریال</td></tr>";
$out = $grid->getGrid();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link type="text/css" href="../css/style.css" rel="stylesheet" />

        <!-- JavaScript Includes -->

        <script type="text/javascript" src="../js/tavanir.js"></script>
        <link type="text/css" href="../js/styles/jquery-ui-1.8.14.css" rel="stylesheet" />
        <script type="text/javascript" src="../js/jquery/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="../js/jquery/jquery.ui.datepicker-cc.all.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>
            سامانه نرم افزاری رزرو آنلاین بهار
        </title>
    </head>
    <body>


        <div align="center">
            <br/>
            <?php echo '<h2>' . $GLOBALS['msg'] . '</h2>' ?>
            <br/>
            <div>
                <form method="get">
                    ازتاریخ : 
                    <input name="sdate" id="sdate" value="<?php echo jdate("Y/m/d",  strtotime($sdate)); ?>"/>
                    تاتاریخ : 
                    <input name="edate" id="edate" value="<?php echo jdate("Y/m/d",  strtotime($edate)); ?>"/>
                    <button>جستجو</button>
                </form>
            </div>
            <?php echo $out; ?>
        </div>
        <script language="javascript">
            var jam = '<?php echo monize($GLOBALS['sum']); ?>';
            var tsum = '<?php echo monize($tsum); ?>';
            $(document).ready(function () {
                $("#jam").html(jam);
                $("#tsum").html(tsum);
                $("#sdate").datepicker({
                    showOn: 'button',
                    dateFormat: 'yy/mm/dd',
                });
                $("#edate").datepicker({
                    showOn: 'button',
                    dateFormat: 'yy/mm/dd',
                });
            });
        </script>
    </body>
</html>

