<?php	session_start();
	include_once("../kernel.php");
	if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        if(!$se->can_view)
              die(lang_fa_class::access_deny);
        function loadAjans($daftar_id,$sel_aj)
        {
                $daftar_id = (int)$daftar_id;
		if($daftar_id>0) //not Admin
                	mysql_class::ex_sql("select `id`,`name` from `ajans`  where `daftar_id`='$daftar_id' and `moeen_id` > 0 and `saghf_kharid`>=".$conf->min_saghf_kharid." order by `name`",$q);
		else//isAdmin
			mysql_class::ex_sql("select `id`,`name` from `ajans`  where `moeen_id` > 0 and `saghf_kharid`>=".$conf->min_saghf_kharid." order by `name`",$q);
                while($r = mysql_fetch_array($q))
                {
                        $sel = (($r['id']==$sel_aj)?'selected="selected"':'');
                        $out.="<option $sel  value='".$r['id']."' >".$r['name']."</option>\n";
                }
                return $out;
        }
	$msg= '';
	$daftar_id = ($se->detailAuth('all'))?-1:$_SESSION['daftar_id'];
	$ajans_id_bes = (isset($_REQUEST['ajans_id1']))?(int)$_REQUEST['ajans_id1']:-1;
	$ajans_id_bed = (isset($_REQUEST['ajans_id2']))?(int)$_REQUEST['ajans_id2']:-1;
	$ghimat = (isset($_REQUEST['ghimat']))?umonize($_REQUEST['ghimat']):0;
	$toz = (isset($_REQUEST['toz']))?$_REQUEST['toz']:'';
	$user_id = $_SESSION['user_id'];
	if(isset($_REQUEST['mod']) && $_REQUEST['mod'] == 'add')
	{
		$out = sanadzan_class::belitSanadzan2($ajans_id_bed,$ajans_id_bes,$user_id,$ghimat,$toz);
		$msg = '<h2 style="color:red;" >ثبت با موفقیت در سند شماره'.$out['shomare_sanad'].' انجام شد</h2>';
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->

		<link type="text/css" href="../css/style.css" rel="stylesheet" />

		<!-- JavaScript Includes -->

		<script type="text/javascript" src="../js/tavanir.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>
			ثبت بلیط تک
		</title>
		<script language="javascript" >
			function addBelit()
			{
				var ghimat = parseInt(document.getElementById('ghimat').value,10);
				var toz = trim(document.getElementById('toz').value);
				if(document.getElementById('ajans_id1').options[document.getElementById('ajans_id1').options.selectedIndex].value== document.getElementById('ajans_id2').options[document.getElementById('ajans_id2').options.selectedIndex].value )
					alert('آژانسها مشابه انتخاب شده اند');
				else 
				{
					if(ghimat>0)
					{
						if(toz!='')
						{
							document.getElementById('mod').value = 'add';
							document.getElementById('frm1').submit();
						}
						else
							alert('توضیحات وارد نشده است');
					}
					else 
						alert('مبلغ را درست وارد کنید');
				}
			}
		</script>
	</head>
	<body>
                <?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>
	<div align="center">
		<form id="frm1" method="POST" >
		<table class="general_div"  >
			<tr>
				<td>آژانس فروشنده بلیت :</td>
				<td><select id="ajans_id1" name="ajans_id1" class="inp" style="width:auto;"  >
						<?php echo loadAjans($daftar_id,$ajans_id1); ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>آژانس خریدار بلیت :</td>
				<td><select id="ajans_id2" name="ajans_id2" class="inp" style="width:auto;"  >
				                <?php echo loadAjans($daftar_id,$ajans_id2); ?>
				        </select>
				</td>
			</tr>
			<tr>
				<td>مبلغ بلیت:</td>
				<td>
					<input onkeyup="monize(this);" type="text" class="inp" id="ghimat" name="ghimat" value="<?php echo $ghimat; ?>" onkeypress="return numbericOnKeypress(event);"/>
				</td>
			</tr>		 
			
			<tr>
				<td>توضیحات:</td>
				<td>
					<input type="text" class="inp" style="width:300px;" name="toz" id="toz" value="<?php echo $toz; ?>" >
				</td>
			</tr>
			<tr>
				<td colspan="2" >
					<input type="button" value="ثبت" onclick="addBelit();" class="inp" />
					<input type="hidden" id="mod" name="mod" value="search" />
				</td>
			</tr>
		</form>
	</div>
		<?php echo $msg; ?>
	</body>
</html>
