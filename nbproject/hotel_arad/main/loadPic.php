<?php
	session_start();
	include_once("../kernel.php");
        if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        if(!$se->can_view)
                die(lang_fa_class::access_deny);
	function loadPic($inp)
	{
		$out = "<img height=\"60px\"  src=\"$inp\" style=\"cursor:pointer;\" onclick=\"wopen('$inp','',500,500);\" />";
		return($out);
	}
	function delete_item($inp)
	{
		$pic = new room_pic_class($inp);
		mysql_class::ex_sqlx("delete from `room_pic` where `id`=$inp");
		unlink($pic->pic);
	}
	$out = '';
	$room_id = isset($_REQUEST['room_id']) ? (int)$_REQUEST['room_id'] : -1;
	if($room_id <= 0)
		die("<script language=\"javascript\">window.close();</script>");
	$rec['room_id'] = $room_id;
        if(isset($_FILES['uploadedfile']))
        {
                $target_path = "room_pic/";
                $target_path = $target_path .$conf->getMoshtari().'_'.$room_id . '_' .  basename( $_FILES['uploadedfile']['name']);
		$ext = explode('.',basename( $_FILES['uploadedfile']['name']));
		$ext = $ext[count($ext)-1];
		$ext = strtolower($ext);
		if(!file_exists($target_path) && ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif'))
		{
	                if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {
				mysql_class::ex_sqlx("insert into `room_pic` (`room_id`,`pic`) values ($room_id,'$target_path')");
        	                $out = "<script> alert('تصویر اضافه شد'); </script>";
                	} else{
                        	$out =  "<script> alert('خطا در ارسال تصویر'); </script>";
	                }
		}
		else
			$out = "<script> alert('نام فایل تکراری است و یا فایل تصویر نمی‌باشد');</script>";
        }
        $grid = new jshowGrid_new("room_pic","grid1");
	$grid->whereClause = " `room_id` = $room_id";
	$grid->setERequest($rec);
        $grid->columnHeaders[0] = null;
	$grid->columnHeaders[1] = null;
	$grid->columnHeaders[2] = 'تصویر';
	$grid->columnFunctions[2] = 'loadPic';
	$grid->deleteFunction = 'delete_item';
	$grid->canAdd = FALSE;
	$grid->canEdit = FALSE;
        $grid->intial();
        $grid->executeQuery();
	$out .= '<form enctype="multipart/form-data" method="POST">
                        <input  type="hidden" name="MAX_FILE_SIZE" value="999999999" />
                        آدرس تصویر : <input name="uploadedfile" class="inp" type="file" />
                        <input class="inp" type="submit" value="ارسال" />
                </form><br/>';
        $out .= $grid->getGrid();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->
		<link type="text/css" href="../js/jquery/themes/trontastic/jquery-ui.css" rel="stylesheet" />
		<link type="text/css" href="../js/jquery/window/css/jquery.window.css" rel="stylesheet" />
		<link type="text/css" href="../css/style.css" rel="stylesheet" />
		<!-- JavaScript Includes -->
		<script type="text/javascript" src="../js/jquery/jquery.js"></script>
		<script type="text/javascript" src="../js/jquery/jquery-ui.js"></script>
		<script type="text/javascript" src="../js/tavanir.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>
			سامانه رزرواسیون هتل	
		</title>
	</head>
	<body>
		<?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>
		<div align="center">
			<br/>
			<br/>
			<?php 
				echo $out;
			?>
		</div>
	</body>
</html>
