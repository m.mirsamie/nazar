<?php
	session_start();
	include("../kernel.php");
	if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        if(!$se->can_view)
                die(lang_fa_class::access_deny);
	$isAdmin = ($se->detailAuth('all') || $se->detailAuth('reserve'));
	function loadFloatHotels($hotel_id)
	{
		$out = '<select name="hotel_id" id="hotel_id" class="inp" >'."\n";
		$hotels = hotel_class::getFloatHotel();
		foreach($hotels as $id=>$name)
			$out .="<option ".(($hotel_id==$id)?'selected="selected"':'')." value=\"$id\" >$name</option>\n";
		$out .= '</select>';
		return $out;
	}
	function loadNumber($inp=2)
	{
		$out = '';
		$inp = (int)$inp;
		for($i=1;$i<32;$i++)
		{
			$sel = (($i==$inp)?'selected="selected"':'');
			$si = $i;
			$out.="<option $sel  value='$i' >$si</option>\n";
		}
		return $out;
	}
	function objChecked($room_typ_id)
	{
		$room_typ_id = (int)$room_typ_id;
		$out = ((isset($_REQUEST['room_'.$room_typ_id]))?' checked="checked" ':'');
		return($out);
	}
	function loadObj($room_typ_id,$name)
	{
		$nm = ((isset($_REQUEST['tedad_'.$room_typ_id]))?(int)$_REQUEST['tedad_'.$room_typ_id]:1);
		$out =
'
<table width="100%">
	<tr>
		<td>
			<input type="checkbox" id="room_'.$room_typ_id.'" name="room_'.$room_typ_id.'" '.objChecked($room_typ_id).'>'.$name.' 
		</td>
		<td width="30px" >
			<select  class="inp" name="tedad_'.$room_typ_id.'" id="tedad_'.$room_typ_id.'" >
				'.loadNumber($nm).'
			</select>
		</td>
	</tr>
</table>
';
		return $out;
	}
	function loadTable($row_count=3)
	{
		$out = "<table width=\"100%\" cellspacing=\"0\" >\n<tr>\n";
		$i = 1;
		mysql_class::ex_sql("select `id`,`name` from `room_typ` order by `name` ",$q);
		while($r = mysql_fetch_array($q))
		{
			$cn = $i % 2;
			$out .= "<td class='room_typ_td$cn' >".loadObj($r['id'],$r['name'])."</td>\n";
			if($i % $row_count == 0)
				$out .= "</tr>\n<tr>\n";
			$i++;	
		}
		$out .= "</tr>\n</table>\n";
		return($out);
	}
	function loadDaftar($inp)
	{
		$inp = (int)$inp;
		$se = security_class::auth((int)$_SESSION['user_id']);
		$isAdmin = ($se->detailAuth('all'));
		$out = "<select name=\"daftar_id\" id=\"daftar_id\" class=\"inp\" style=\"width:auto;\" onchange=\"document.getElementById('mod').value='2';document.getElementById('frm1').submit();\" ><option value='0' ></option>";
		if($isAdmin)
			mysql_class::ex_sql('select `id`,`name` from `daftar` where `kol_id`>0 order by `name` ',$q);
		else
			mysql_class::ex_sql('select `id`,`name` from `daftar` where `id`='.$inp.' and `kol_id`>0 order by `name`',$q);
		while($r = mysql_fetch_array($q))
		{
			$sel = (($r['id']==$inp)?'selected="selected"':'');
			$out.="<option $sel  value='".$r['id']."' >".$r['name']."</option>\n";
		}
		$out.='</select>';
		return $out;	
	}
	function loadDaftarBelit($inp,$typ)
	{
		$inp = (int)$inp;
		$user = new user_class((int)$_SESSION['user_id']);
		$se = security_class::auth((int)$_SESSION['user_id']);
		$isAdmin = ($se->detailAuth('all'));
		$out = "<select name=\"daftar_idBelit_$typ\" id=\"daftar_idBelit_$typ\" class=\"inp\" style=\"width:auto;\" onchange=\"document.getElementById('mod').value='2';document.getElementById('frm1').submit();\" ><option value='0' ></option>";
		if($isAdmin)
			mysql_class::ex_sql('select `id`,`name` from `daftar` where `kol_id`>0 order by `name` ',$q);
		else
			mysql_class::ex_sql('select `id`,`name` from `daftar` where `id`='.$user->daftar_id.' and `kol_id`>0 order by `name`',$q);
		while($r = mysql_fetch_array($q))
		{
			$sel = (($r['id']==$inp)?'selected="selected"':'');
			$out.="<option $sel  value='".$r['id']."' >".$r['name']."</option>\n";
		}
		$out.='</select>';
		return $out;	
	}
	function loadAjans($daftar_id,$sel_aj)
	{
		$daftar_id = (int)$daftar_id;
		$out = "<select id='ajans_id' name=\"ajans_id\" class=\"inp\" style=\"width:auto;\"  >";
		//mysql_class::ex_sql("select `id`,`name` from `ajans`  where `daftar_id`='$daftar_id' and `moeen_id` > 0 and `saghf_kharid`>=".$conf->min_saghf_kharid." order by `name`",$q);
		$ajanses = ajans_class::loadByDaftar($daftar_id,TRUE);
		//var_dump($ajanses);
		for($i=0;$i<count($ajanses);$i++)
		{
			$sel = (($ajanses[$i]['id']==$sel_aj)?'selected="selected"':'');
			$out.="<option $sel  value='".$ajanses[$i]['id']."' >".$ajanses[$i]['name']."</option>\n";
		}
		$out.='</select>';
		return $out;
	}
	function loadAjansBelit($daftar_id,$sel_aj,$typ)
	{
		$daftar_id = (int)$daftar_id;
		$out = "<select id='ajans_idBelit_$typ' name=\"ajans_idBelit_$typ\" class=\"inp\" style=\"width:auto;\"  >";
		$conf = new conf;
		if($conf->ajans_saghf_mande)
			mysql_class::ex_sql("select `id`,`name` from `ajans`  where `daftar_id`='$daftar_id' and `moeen_id` > 0 and `saghf_kharid`>=".$conf->min_saghf_kharid." order by `name`",$q);
		else
			mysql_class::ex_sql("select `id`,`name` from `ajans`  where `daftar_id`='$daftar_id' and `moeen_id` > 0 order by `name`",$q);
		while($r = mysql_fetch_array($q))
		{
			$sel = (($r['id']==$sel_aj)?'selected="selected"':'');
			$out.="<option $sel  value='".$r['id']."' >".$r['name']."</option>\n";
		}
		$out.='</select>';
		return $out;
	}
	$aztarikh = ((isset($_REQUEST['aztarikh']))?audit_class::hamed_pdateBack($_REQUEST['aztarikh']):date('Y-m-d 14:00:00'));
	$shab = ((isset($_REQUEST['shab']))?(int)$_REQUEST['shab']:1);
	$tatarikh = date("Y-m-d H:i:s",strtotime($aztarikh.' + '.$shab.' day'));
	$sel_aj = ((isset($_REQUEST['ajans_id']))?$_REQUEST['ajans_id']:0);
	$hotel_id = (isset($_REQUEST['hotel_id']))?$_REQUEST['hotel_id']:-1;
	$ajans_idBelit_1 = ((isset($_REQUEST['ajans_idBelit_1']))?(int)$_REQUEST['ajans_idBelit_1']:-1);
	$ajans_idBelit_2 = ((isset($_REQUEST['ajans_idBelit_2']))?(int)$_REQUEST['ajans_idBelit_2']:-1);
	$ajans_idBelit_3 = ((isset($_REQUEST['ajans_idBelit_3']))?(int)$_REQUEST['ajans_idBelit_3']:-1);
	$form = '';
	if($isAdmin )
	{
		$daftar_id = ((isset($_REQUEST['daftar_id']))?(int)$_REQUEST['daftar_id']:-1);
		$daftar_idBelit_1 = ((isset($_REQUEST['daftar_idBelit_1']))?(int)$_REQUEST['daftar_idBelit_1']:-1);
		$daftar_idBelit_2 = ((isset($_REQUEST['daftar_idBelit_2']))?(int)$_REQUEST['daftar_idBelit_2']:-1);
		$daftar_idBelit_3 = ((isset($_REQUEST['daftar_idBelit_3']))?(int)$_REQUEST['daftar_idBelit_3']:-1);
	}
	else
	{
		$daftar_id = (int)$_SESSION["daftar_id"] ;
		$daftar_idBelit_1 = (int)$_SESSION["daftar_id"] ;
		$daftar_idBelit_2 = (int)$_SESSION["daftar_id"] ;
		$daftar_idBelit_3 = (int)$_SESSION["daftar_id"] ;
	}
	if($conf->tour_enabled)
	{
		$tour_mab_view = 'مبلغ تور:';
		$raft_sherkat = 'شرکت بلیت رفت';
		$m_belit1_view = 'مبلغ  بلیــت  رفت:';
		$m_belit2_view = '<br/> مبلغ‌بلیت‌برگشت:';
		$m_belit3_view = '<br/> مبـلغ‌ کمیـسیـون:';
		$m_belit2_style = '';
		$m_belit3_style = '';
	}
	else
	{
		$tour_mab_view = 'مبلغ کل هتل:';
		$raft_sherkat = 'حساب کمیسیون';
		$m_belit1_view = 'مبلغ  کمیسیون:';
		$m_belit2_view = '';
		$m_belit3_view = '';
		$m_belit2_style = 'style="display:none;"';
		$m_belit3_style = 'style="display:none;"';
	}
	if(isset($_REQUEST['mod']) && $_REQUEST['mod']=='reserve')
	{
		$hotel_name = $_REQUEST['hotel_name'];
		$hotel_id = (int)$_REQUEST['hotel_id'];
		$aztarikh = $_REQUEST['aztarikh'];
		$shab = (int)$_REQUEST['shab'];
		$shabreserve = ((isset($_REQUEST['shabreserve']))?TRUE:FALSE);
		$roozreserve = ((isset($_REQUEST['roozreserve']))?TRUE:FALSE);
		$room_typs = null;
		foreach($_REQUEST as $key => $value)
		{
			$tmp = explode('_',$key);
			if($tmp[0] == 'room' && count($tmp)==2)
				$room_typs[(int)$tmp[1]] = (int)$_REQUEST['tedad_'.(int)$tmp[1]];
		}
		$kh_sobhane_txt =$_REQUEST['kh_txt_1'] ;
		$kh_sobhane_v = (isset($_REQUEST['kh_v_1'])?TRUE:FALSE);
		$kh_sobhane_kh = (isset($_REQUEST['kh_kh_1'])?TRUE:FALSE);
		$kh_nahar_txt =$_REQUEST['kh_txt_2'] ;
		$kh_nahar_v = (isset($_REQUEST['kh_v_2'])?TRUE:FALSE);
		$kh_nahar_kh = (isset($_REQUEST['kh_kh_2'])?TRUE:FALSE);
		$kh_sham_txt =$_REQUEST['kh_txt_3'] ;
		$kh_sham_v = (isset($_REQUEST['kh_v_3'])?TRUE:FALSE);
		$kh_sham_kh = (isset($_REQUEST['kh_kh_3'])?TRUE:FALSE);
		$kh_transfer_ch =(isset($_REQUEST['kh_ch_4'])?TRUE:FALSE);
		$kh_transfer_v = (isset($_REQUEST['kh_v_4'])?TRUE:FALSE);
		$kh_transfer_kh =(isset($_REQUEST['kh_kh_4'])?TRUE:FALSE);
		if($hotel_name=='')
		{
			$hot = new hotel_class($hotel_id);
			$hotel_name = $hot->name;
		}
		$new_hotel = hotel_class::add($hotel_name);
		$hotel_id = $new_hotel['hotel_id'];	
		$room_ids = array();
		foreach($room_typs as $room_typ_id=>$tedad)
			$room_ids[] = room_class::add($hotel_id,$room_typ_id,$tedad,$aztarikh,$tatarikh);
		$form = "
<div style='display:none;' >
	<form id=\"reserve2\" action=\"reserve2.php\" method=\"GET\">
		هتل:
<input name='hotel_id' value='$hotel_id' >
		از تاریخ:
<input name='aztarikh' value='$aztarikh' >
		شب:
<input name='shab' value='$shab' >
		مد
<input name='mod' value='1' >
		مد۲
<input name='mode1' value='0' >
		تعداد نفرات
<input name='tedad_nafarat' value='".$_REQUEST['tedad_nafarat']."' >
		دفتر:
<input name='daftar_id' value='$daftar_id' >
		آژانس
<input name='ajans_id' value='$sel_aj' >
		دفتر بلیط۱
<input name='daftar_idBelit_1' value='$daftar_idBelit_1' >
		دفتر بلیط۲
<input name='daftar_idBelit_2' value='$daftar_idBelit_2' >
		دفتر بلیط۳
<input name='daftar_idBelit_3' value='$daftar_idBelit_3' >
		آژانس بلیط۱
<input name='ajans_idBelit_1' value='$ajans_idBelit_1' >
		آژانس بلیط۲
<input name='ajans_idBelit_2' value='$ajans_idBelit_2' >
		آژانس بلیط۳
<input name='ajans_idBelit_3' value='$ajans_idBelit_3' >
		صبحانه
<input name='kh_txt_".$new_hotel['sobhane']."' value='$kh_sobhane_txt' >
		<input name='kh_v_".$new_hotel['sobhane']."' type='checkbox' ".(($kh_sobhane_v)?'checked=checked':'')." >
		<input name='kh_kh_".$new_hotel['sobhane']."' type='checkbox' ".(($kh_sobhane_kh)?'checked=checked':'')." >

		ناهار
<input name='kh_txt_".$new_hotel['nahar']."' value='$kh_nahar_txt' >
		<input name='kh_v_".$new_hotel['nahar']."' type='checkbox' ".(($kh_nahar_v)?'checked=checked':'')." >
		<input name='kh_kh_".$new_hotel['nahar']."' type='checkbox' ".(($kh_nahar_kh)?'checked=checked':'')." >

		شام
<input name='kh_txt_".$new_hotel['sham']."' value='$kh_sham_txt' >
		<input name='kh_v_".$new_hotel['sham']."' type='checkbox' ".(($kh_sham_v)?'checked=checked':'')." >
		<input name='kh_kh_".$new_hotel['sham']."' type='checkbox' ".(($kh_sham_kh)?'checked=checked':'')." >

		ترانسفر
<input name='kh_ch_".$new_hotel['transfer']."' type='checkbox' ".(($kh_transfer_ch)?'checked=checked':'')." >
		<input name='kh_v_".$new_hotel['transfer']."' type='checkbox' ".(($kh_transfer_v)?'checked=checked':'')." >
		<input name='kh_kh_".$new_hotel['transfer']."' type='checkbox' ".(($kh_transfer_kh)?'checked=checked':'')." >

		مبلغ
<input name='tour_mablagh' value='".$_REQUEST['tour_mablagh']."' >
		مبلغ بلیط
<input name='belit_mablagh_1' value='".$_REQUEST['belit_mablagh_1']."' >
		مبلغ بلیط۲
<input name='belit_mablagh_2' value='".((isset($_REQUEST['belit_mablagh_2']))?$_REQUEST['belit_mablagh_2']:'')."' >
		مبلغ بلیط۳(کمیسیون)
<input name='belit_mablagh_3' value='".((isset($_REQUEST['belit_mablagh_3']))?$_REQUEST['belit_mablagh_3']:'')."' >
		اتاقها
<input name='room_id_tmp' value='".(implode(',',$room_ids))."' >
	</form>
</div>
<script language='javascript' >document.getElementById('reserve2').submit();</script>
";
	}
	$tr_nafar = "
<table width='100%'  />
	<tr>
		<td>تعدادنفرات:<input onkeypress='return numbericOnKeypress(event);' type='text' id='tedad_nafarat' name='tedad_nafarat' style='width:30px;' value='".((isset($_REQUEST['tedad_nafarat']))?$_REQUEST['tedad_nafarat']:1)."' class='inp' onblur='calculate_nafar();' ></td>
		<td colspan='3' >
			$tour_mab_view<input onkeyup='monize(this);' class='inp' type='text' name='tour_mablagh' id='tour_mablagh' value='".((isset($_REQUEST['tour_mablagh']))?$_REQUEST['tour_mablagh']:"")."' >
		</td>
		<td>
			$m_belit1_view<input onkeyup='monize(this);' class='inp' type='text' name='belit_mablagh_1' id='belit_mablagh_1' value='".((isset($_REQUEST['belit_mablagh_1']))?$_REQUEST['belit_mablagh_1']:"")."' >
		$m_belit2_view<input $m_belit2_style onkeyup='monize(this);' class='inp' type='text' name='belit_mablagh_2' id='belit_mablagh_2' value='".((isset($_REQUEST['belit_mablagh_2']))?$_REQUEST['belit_mablagh_2']:"")."' >
		$m_belit3_view<input $m_belit2_style onkeyup='monize(this);' class='inp' type='text' name='belit_mablagh_3' id='belit_mablagh_3' value='".((isset($_REQUEST['belit_mablagh_3']))?$_REQUEST['belit_mablagh_3']:"")."' >
		</td>
	</tr>
</table>

";
	$tr_hesab = "<table width='100%' style='border:dotted 1px #000000;margin-top:2px;' ><tr><td align='left' >نام دفتر:</td><td>".loadDaftar($daftar_id)."</td><td align='left'  >نام آژانس</td><td colspan='1' >". loadAjans($daftar_id,$sel_aj)."</td>";
			$sel_ajBelit_1 = ((isset($_REQUEST['ajans_idBelit_1']))?$_REQUEST['ajans_idBelit_1']:0);
			$tr_hesab .= "<tr><td align='left'>دفتر:</td><td>".loadDaftarBelit($daftar_idBelit_1,1)."</td><td align='left' >$raft_sherkat</td><td colspan='1' >". loadAjansBelit($daftar_idBelit_1,$sel_ajBelit_1,1)."</td>";
			$tr_hesab .= "</tr>";
			$sel_ajBelit_2 = ((isset($_REQUEST['ajans_idBelit_2']))?$_REQUEST['ajans_idBelit_2']:0);
			$tr_hesab .= "<tr $m_belit2_style ><td align='left' >دفتر:</td><td>".loadDaftarBelit($daftar_idBelit_2,2)."</td><td align='left' >شرکت بلیت برگشت</td><td colspan='1' >". loadAjansBelit($daftar_idBelit_2,$sel_ajBelit_2,2)."</td>";
			$sel_ajBelit_3 = ((isset($_REQUEST['ajans_idBelit_3']))?$_REQUEST['ajans_idBelit_3']:0);
			$tr_hesab .= "<tr $m_belit2_style ><td align='left' >دفتر:</td><td>".loadDaftarBelit($daftar_idBelit_3,3)."</td><td align='left' >کمیسیون</td><td colspan='1' >". loadAjansBelit($daftar_idBelit_3,$sel_ajBelit_3,3)."</td>";
			$tr_hesab .= "</tr></table>";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link type="text/css" href="../css/style.css" rel="stylesheet" />
		<link type="text/css" href="../js/styles/jquery-ui-1.8.14.css" rel="stylesheet" />
		<script type="text/javascript" src="../js/tavanir.js"></script>
		<script type="text/javascript" src="../js/jquery/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../js/jquery/jquery.ui.datepicker-cc.all.min.js"></script>
		<script type="text/javascript">
			$(function() {
			//-----------------------------------
			// انتخاب با کلیک بر روی عکس
			$("#aztarikh").datepicker({
			    showOn: 'button',
			    dateFormat: 'yy/mm/dd',
			    buttonImage: '../js/styles/images/calendar.png',
			    buttonImageOnly: true
			});
			});
			function reserve()
			{
				if(checkboxChecked())
				{
					if(confirm('آیا رزرو انجام شود؟'))
					{
						document.getElementById('mod').value = 'reserve';
						document.getElementById('frm1').submit();
					}
				}
				else
					alert('لطفاً اطلاعات را کامل وارد کنید');
			}
			function clearKhadamat()
			{
				var inps = document.getElementsByTagName('input');
				var x;
				for(var i=0;i < inps.length;i++)
				{
					x = inps[i].id.split('_');
					if(x[0]=='kh' && x.length==3)
					{
						if(x[1]=='txt')
							inps[i].value=0;	
						if(x[1]=='v' || x[1]=='kh' || x[1]=='ch')
							inps[i].checked=false;
					}
				}
			}
			function resetOrNotKh(Obj)
			{
				if(Obj.checked)
					reset_full_board();
				else
					clearKhadamat();
			
			}
			function reset_full_board()
			{ 
				document.getElementById('kh_v_1').checked=false;
				document.getElementById('kh_kh_1').checked=true;
				document.getElementById('kh_v_2').checked=false;
				document.getElementById('kh_kh_2').checked=true;
				document.getElementById('kh_v_3').checked=true;
				document.getElementById('kh_kh_3').checked=false;
				document.getElementById('kh_v_4').checked=true;
				document.getElementById('kh_kh_4').checked=true;
				kh_check("4");
				calculate_nafar();
			}
			function kh_check(inp)
			{
				var mainObj = document.getElementById('kh_ch_'+inp);
				var vObj = document.getElementById('kh_v_'+inp);
				var khObj = document.getElementById('kh_kh_'+inp);
				if(vObj.checked || khObj.checked )
					mainObj.checked = true;
				else
					mainObj.checked = false;
			}
			function calculate_nafar()
			{
				if(document.getElementById('tedad_nafarat'))
				{
		                        var adult = document.getElementById('tedad_nafarat').value;   
		                        var inps =  document.getElementsByTagName('input');
		                        var tmp;
		                        adult = parseInt(adult,10);
		                        for (var i=0;i<inps.length;i++)
		                        { 
		                                tmp = inps[i].name.split('_');
		                                if (tmp.length==3 && tmp[0]=='kh' && tmp[1]=='txt')
		                                        inps[i].value = String(adult);
		                        }
				}
			}
			function checkboxChecked()
			{
				var out = false;
				var tmp;
                                var inps = document.getElementsByTagName('input');
				var tour = parseInt(unFixNums(umonize(document.getElementById('tour_mablagh').value)),10);
				if(isNaN(tour))
					tour =0;
				var belit1 = parseInt(unFixNums(umonize(document.getElementById('belit_mablagh_1').value)),10);
				if(isNaN(belit1))
					belit1 =0;
				var belit2 = parseInt(unFixNums(umonize(document.getElementById('belit_mablagh_2').value)),10);
				if(isNaN(belit2))
					belit2 =0;
				var belit3 = parseInt(unFixNums(umonize(document.getElementById('belit_mablagh_3').value)),10);
				if(isNaN(belit3))
					belit3 =0;
                                for(var i=0;i < inps.length;i++)
				{
					tmp = String(inps[i].id).split('_');
                                        if(tmp[0]=='room' && inps[i].type=='checkbox' && inps[i].checked && tmp.length==2)
                                                out = true;
				}
				if(document.getElementById('daftar_id').selectedIndex <= 0 ||
 document.getElementById('tour_mablagh').value=='' ||
 (parseInt(document.getElementById('belit_mablagh_1').value,10)>0 && document.getElementById('daftar_idBelit_1').selectedIndex <= 0) ||
 (parseInt(document.getElementById('belit_mablagh_2').value,10)>0 && document.getElementById('daftar_idBelit_2').selectedIndex <= 0) ||
(parseInt(document.getElementById('belit_mablagh_3').value,10)>0 && document.getElementById('daftar_idBelit_3').selectedIndex <= 0) ||
 parseInt(document.getElementById('tour_mablagh').value,10)==0 || 
tour<=(belit1+belit2+belit3)
 )
                                        out = false;
				return(out);
			}
		</script>
		<style>
			.room_typ_td0 { border:none;padding:5px;background-color:#ffffff;text-align : right;}
			.room_typ_td1 { border:none;padding:5px;background-color:#efefef;text-align : right;}
			th,td.names
			{	
				border: 1px dotted black;
				text-align : center;
				padding: 3px;
			}
			table.head1
			{
				margin:5px;
			}
			table #fake td
			{
				border: 1px dotted black;
				padding: 3px;
			}
		</style>
		<title>
			سامانه رزرواسیون بهار ، رزرو شناور
		</title>
	<script>
		function st()
		{
		week= new Array("يكشنبه","دوشنبه","سه شنبه","چهارشنبه","پنج شنبه","جمعه","شنبه")
		months = new Array("فروردين","ارديبهشت","خرداد","تير","مرداد","شهريور","مهر","آبان","آذر","دي","بهمن","اسفند");
		a = new Date();
		d= a.getDay();
		day= a.getDate();
		var h=a.getHours();
      		var m=a.getMinutes();
  		var s=a.getSeconds();
		month = a.getMonth()+1;
		year= a.getYear();
		year = (year== 0)?2000:year;
		(year<1000)? (year += 1900):true;
		year -= ( (month < 3) || ((month == 3) && (day < 21)) )? 622:621;
		switch (month) 
		{
			case 1: (day<21)? (month=10, day+=10):(month=11, day-=20); break;
			case 2: (day<20)? (month=11, day+=11):(month=12, day-=19); break;
			case 3: (day<21)? (month=12, day+=9):(month=1, day-=20); break;
			case 4: (day<21)? (month=1, day+=11):(month=2, day-=20); break;
			case 5:
			case 6: (day<22)? (month-=3, day+=10):(month-=2, day-=21); break;
			case 7:
			case 8:
			case 9: (day<23)? (month-=3, day+=9):(month-=2, day-=22); break;
			case 10:(day<23)? (month=7, day+=8):(month=8, day-=22); break;
			case 11:
			case 12:(day<22)? (month-=3, day+=9):(month-=2, day-=21); break;
			default: break;
		}
		//document.write(" "+week[d]+" "+day+" "+months[month-1]+" "+ year+" "+h+":"+m+":"+s);
			var total=" "+week[d]+" "+day+" "+months[month-1]+" "+ year+" "+h+":"+m+":"+s;
			    document.getElementById("tim").innerHTML=total;
   			    setTimeout('st()',500);
		}
		</script>
	</head>
	<body onload='st()'>
		<center>
		<span id='tim' >test2
		</span>
		</center>
		<?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>	
		<div align="right" style="padding-right:30px;padding-top:10px;">
			<a href="help.php" target="_blank"><img src="../img/help.png"/></a>
		</div>
		<div align="center">
			<form id='frm1'  method='POST' >
				<table cellspacing="0" class="head1" >
					<tr>
						<th>نام هتل</th>
						<th>تاریخ</th>
						<th>مدت اقامت</th>
						<th>شب-‌رزرو<br/>(نیم شارژ ورودی)</th>
						<th>روز-‌رزرو<br/>(نیم شارژ خروجی)</th>
					</tr>
					<tr class='names'>
						<td class='names' >
							<input class="inp" name="hotel_name" id="hotel_name" value="<?php echo ((isset($_REQUEST['hotel_name']))?$_REQUEST['hotel_name']:'') ?>" >
							<?php echo loadFloatHotels($hotel_id); ?>
						</td>
						<td class='names' >	
		 					   <input value=" <?php echo ((isset($_REQUEST['aztarikh']))?audit_class::hamed_pdate($aztarikh):audit_class::hamed_pdate(date("Y-m-d H:i:s"))); ?>" type="text" name='aztarikh' readonly='readonly' class='inp' style='direction:ltr;' id="aztarikh" />	
						</td>
						<td class='names' >
							<select  class='inp' name='shab' id='shab' >
								<?php  echo loadNumber((isset($_REQUEST['shab']))?$_REQUEST['shab']:1); ?>
							</select>						
						</td>
						<td class='names' >
							<input name="shabreserve" id="shabreserve" type="checkbox" <?php echo ((isset($_REQUEST['shabreserve']))?'checked="checked"':''); ?> >
						</td>
						<td class='names' >
							<input name="roozreserve" id="roozreserve" type="checkbox" <?php echo ((isset($_REQUEST['roozreserve']))?'checked="checked"':''); ?> >
						</td>
					</tr>
					<tr>
						<td colspan="5" class='room_typ_td' >
							<?php echo loadTable(); ?>
						</td>
					</tr>
					<tr>
						<td colspan="5" class='names' >
                                                   	<?php echo $tr_nafar; ?>     
                                                </td>
					</tr>
					<tr>
						<td colspan="5" >
                                                   	<?php echo $tr_hesab; ?>     
                                                </td>
					</tr>
					<tr>
                                                <td colspan="5" class='room_typ_td' >
							<?php echo khadamat_class::loadFake(); ?>
                                                </td>
                                        </tr>
					<tr>
                                                <td colspan="5" class='names' >
							<input type='hidden' name='mod' id='mod' value='1' >
                                                        <input type="button" value="رزرو" onclick="reserve();" class="inp" style="font-size:15px;width:60px;font-weight:bold;" />
                                                </td>
                                        </tr>
				</table>
			</form>
			<?php
				echo $form;
			?>
		</div>
		<script language="javascript" >
			calculate_nafar();
		</script>
	</body>
</html>
