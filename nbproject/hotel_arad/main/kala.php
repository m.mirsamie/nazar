<?php	session_start();
	include_once("../kernel.php");
	if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        if(!$se->can_view || !$conf->anbar)
                die(lang_fa_class::access_deny);
	function loadKala()
	{
		$out = null;
		mysql_class::ex_sql('select `name`,`id` from `kala_no` order by `name`',$q);
		while($r = mysql_fetch_array($q))
		{
			$out[$r['name']] = (int)$r['id'];
		}
		return($out);
	}
	function loadVahed()
	{
		$out = null;
		mysql_class::ex_sql('select `name`,`id` from `kala_vahed` order by `name`',$q);
		while($r = mysql_fetch_array($q))
		{
			$out[$r['name']] = (int)$r['id'];
		}
		return($out);
	}
	function loadMojoodi($inp)
	{
		$out=anbar_det_class::getMojoodi($inp);
		if($out['out']<=0)
			$out = $out['msg'];
		else
		{
			$out = $out['out'];
			$out = "<span style='color:blue;cursor:pointer;' onclick='wopen(\"kala_gozaresh.php?kala_id=$inp\",\"\",700,400);' ><u>$out</u></span>";
		}
		
		return $out;
	}
	function add_item()
	{
		foreach($_REQUEST as $key => $value)
                        if(substr($key,0,4)=="new_")
                                if($key != "new_en" && $key != "new_id")
                                        $fields[substr($key,4)] =perToEnNums($value);
                $fi = "(";
	        $valu="(";
		foreach ($fields as $field => $value)
        	{
		        $fi.="`$field`,";
                        $valu .="'$value',";
                }
       		$fi=substr($fi,0,-1);
                $valu=substr($valu,0,-1);
		$fi.=")";
        	$valu.=")";
        	$query="insert into `kala` $fi values $valu";
		mysql_class::ex_sqlx($query);
	}
	$grid = new jshowGrid_new("kala","grid1");
	$grid->whereClause=" 1=1 order by `code`,`kala_no_id`,`name`";
	$grid->columnHeaders[0] = 'موجودی';
	$grid->columnFunctions[0] = 'loadMojoodi';
	$grid->columnAccesses[0] = 0 ;
        $grid->columnHeaders[1] = "نام کالا";
	$grid->columnFilters[1] = TRUE;
	$grid->columnHeaders[2] = "کد کالا";
	$grid->columnHeaders[3] = "نوع کالا";
	$grid->columnLists[3] = loadKala();
	$grid->columnHeaders[4] = 'واحد';
	$grid->columnLists[4] = loadVahed();
	$grid->columnHeaders[5] = null;
	$grid->gotoLast = TRUE;
	$grid->addFunction = 'add_item';
        $grid->intial();
   	$grid->executeQuery();
        $out = $grid->getGrid();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->
		<link type="text/css" href="../js/jquery/themes/trontastic/jquery-ui.css" rel="stylesheet" />
		<link type="text/css" href="../js/jquery/window/css/jquery.window.css" rel="stylesheet" />

		<link type="text/css" href="../css/style.css" rel="stylesheet" />
		<!-- JavaScript Includes -->
		<script type="text/javascript" src="../js/tavanir.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>
		</title>
	</head>
	<body>
		<?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>
		<div align="right" style="padding-right:30px;padding-top:10px;">
			<a href="help.php" target="_blank"><img src="../img/help.png"/></a>
		</div>
		<div align="center">
			<br/>
			<br/>
			<?php	echo $out;?>
		</div>
		<script language="javascript" >
			if(document.getElementById('new_id'))
				document.getElementById('new_id').style.display= 'none';
		</script>
	</body>
</html>
