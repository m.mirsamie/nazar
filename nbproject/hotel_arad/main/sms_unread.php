<?php
	session_start();
	include("../kernel.php");
        if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        if(!$se->can_view)
                die(lang_fa_class::access_deny);
	function hamed_pdate($str)
        {
                $out=jdate('Y/n/j',strtotime($str));
                return $out;
        }
	$GLOBALS['notSent'] = 0;
	$GLOBALS['noReply'] = 0;
	$GLOBALS['excellent'] = 0;
	$GLOBALS['good'] = 0;
	$GLOBALS['meduim'] = 0;
	$GLOBALS['low'] = 0;
	$GLOBALS['unknown'] = 0;
	function loadNazar($inp)
	{
		$inp = trim($inp);
		$out = '';
		switch ($inp)
		{
			case -2:
				$out ='پیامک‌ارسال‌نشده‌است';
				$GLOBALS['notSent']++;
				break;
			case -1:
				$out ='میهمان‌پاسخ‌نداده‌است';
				$GLOBALS['noReply']++;
				break;
			case 1:
				$out ='عالی';
				$GLOBALS['excellent']++;
				break;
			case 2:
				$out ='خوب';
				$GLOBALS['good']++;
				break;
			case 3:
				$out ='متوسط';
				$GLOBALS['meduim']++;
				break;
			case 4:
				$out ='ضعیف';
				$GLOBALS['low']++;
				break;
			default :
				$out = $inp;
				$GLOBALS['unknown']++;
				break;
		}
		return ($out);
	}
	function hpdate($inp)
	{
		return(audit_class::hamed_pdate($inp));
	}
	$grid = new jshowGrid_new("in_sms","grid1");
	$grid->whereClause = " `en`='0'";
	$grid->columnHeaders[0]= null;
	$grid->columnHeaders[1]= 'ارسال کننده';
	$grid->columnHeaders[2]= 'نظر';
	$grid->columnFunctions[2]= "loadNazar";
	$grid->columnHeaders[3]= null;
	$grid->columnHeaders[4]= 'تاریخ';
	$grid->columnFunctions[4]= "hpdate";
	$grid->canAdd = FALSE;
	$grid->canDelete = FALSE;
	$grid->canEdit = FALSE;
	$grid->intial();
	$grid->executeQuery();
	$out = $grid->getGrid();
?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link type="text/css" href="../css/style.css" rel="stylesheet" />

		<link type="text/css" href="../js/styles/jquery-ui-1.8.14.css" rel="stylesheet" />
		<script type="text/javascript" src="../js/jquery/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../js/jquery/jquery.ui.datepicker-cc.all.min.js"></script>
		<script type="text/javascript" src="../js/tavanir.js"></script>
		</script>
		<style type="text/css" >
			td{text-align:center;}
		</style>
		<title>
			پیامک های خوانده نشده
		</title>
	</head>
	<body>
                <?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>
		<div align="right" style="padding-right:30px;padding-top:10px;">
			<a href="help.php" target="_blank"><img src="../img/help.png"/></a>
		</div>
		<div align="center">
			<br/>
			<br/>			
			<br/>
			<?php echo $out; ?>
		</div>
	</body>
</html>
