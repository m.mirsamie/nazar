<?php
	session_start();
	include_once('../kernel.php');
	if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        if(!$se->can_view)
                die(lang_fa_class::access_deny);
	$is_admin = $se->detailAuth('all'); 
	$h_id = ((isset($_REQUEST['h_id']))?(int)$_REQUEST['h_id']:-1);	
	$r1 = ((isset($_REQUEST['r1']))?(int)$_REQUEST['r1']:'');
        $r2 = ((isset($_REQUEST['r2']))?(int)$_REQUEST['r2']:'');
	//echo "<br>r1=$r1<br/>r2=$r2";
	$reserver1 = '';
	$reserver2 = '';
	$azt1 = ((isset($_REQUEST['azt1']))?audit_class::hamed_pdateBack($_REQUEST['azt1']):'');
	$azt2 = ((isset($_REQUEST['azt2']))?audit_class::hamed_pdateBack($_REQUEST['azt2']):'');
	$tat1 = ((isset($_REQUEST['tat1']))?audit_class::hamed_pdateBack($_REQUEST['tat1']):'');
	$tat2 = ((isset($_REQUEST['tat2']))?audit_class::hamed_pdateBack($_REQUEST['tat2']):'');
	$jday = ((isset($_REQUEST['jday']))?audit_class::hamed_pdateBack($_REQUEST['jday']):date("Y-m-d H:i:s"));
	$mod =  ((isset($_REQUEST['mod']))?$_REQUEST['mod']:'');
	$room_id1 = ((isset($_REQUEST['room_id1']))?(int)$_REQUEST['room_id1']:-1);
	$room_id2 = ((isset($_REQUEST['room_id2']))?(int)$_REQUEST['room_id2']:-1);
	$room_name1 = '';
	$room_name2 = '';
	$can_change = FALSE;
	$mg = '';
	$msg = '';
	switch($mod)
	{
		case 'select':
			if($r1 >0)
			{
				$re1 = new hotel_reserve_class;
				$re1->loadByReserve($r1);
				$reserver1 = $re1->lname;
				$re1 = new room_det_class;
				$re1 = $re1->loadByReserve($r1);
				$azt1 = $re1[0][0]->aztarikh;
                	        $tat1 = $re1[0][0]->tatarikh;
			}
			if($r2 >0)
			{
	                        $re1 = new room_det_class;
        	                $re1 = $re1->loadByReserve($r2);
                	        $azt2 = $re1[0][0]->aztarikh;
                        	$tat2 = $re1[0][0]->tatarikh;
				$re1 = new hotel_reserve_class;
        	                $re1->loadByReserve($r2);
				$reserver2 = $re1->lname;
			}
			$room1 = new room_class($room_id1);
			$room_name1 = $room1->name;
			$room1 = new room_class($room_id2);
                        $room_name2 = $room1->name;
			if($room_name1 != '' && $room_name2 != '')
				$can_change = TRUE;
			break;
		case 'change':
			$done = room_det_class::changeDate($room_id1,$room_id2,$jday,$r1);
			if($done)
				$msg = "unchange();alert('جابجایی با موفقیت انجام پذیرفت');";
			else
				$msg = "alert('جابجایی امکان پذیر نیست');";
			break;
	}
        $d = ((isset($_REQUEST['d']))?$_REQUEST['d']:perToEnNums(jdate("m")));
        $month = array('فروردین','اردیبهشت','خرداد','تیر','مرداد','شهریور','مهر','آبان','آذر','دی','بهمن','اسفند');
        $da = audit_class::hamed_pdateBack(jdate("Y/$d/d"));
        $tmp = explode(" ",$da);
        $da = $tmp[0];
        $hotel1 = new hotel_class($h_id);
	$hotel1->setRoomJavaScript = TRUE;
        $outvazeat = $hotel1->loadRooms($da,$is_admin,'select_reserve');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link type="text/css" href="../css/style.css" rel="stylesheet" />

		<link type="text/css" href="../js/styles/jquery-ui-1.8.14.css" rel="stylesheet" />
		<script type="text/javascript" src="../js/jquery/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../js/jquery/jquery.ui.datepicker-cc.all.min.js"></script>
		<script type="text/javascript" src="../js/tavanir.js"></script>
		<script type="text/javascript">
			function addReserve(rid,room_id)
			{
				if(document.getElementById('r1').value == '' && document.getElementById('r2').value!=rid)
				{
					document.getElementById('r1').value = rid;
					document.getElementById('room_id1').value = room_id;
					document.getElementById('d1').style.display = '';
				}
				else if(document.getElementById('r2').value == '' && document.getElementById('r1').value!=rid)
				{
					document.getElementById('r2').value = rid;
                                        document.getElementById('room_id2').value = room_id;
					document.getElementById('d2').style.display = '';
				}
				if(parseInt(document.getElementById('room_id1').value,10)>0 && parseInt(document.getElementById('room_id2').value,10)>0)
					return(true);
				else
					return(false);
			}
			function select_reserve(rid,room_id)
			{
				if(addReserve(rid,room_id))
				{
					document.getElementById('loading').style.display='';
					document.getElementById('frm1').submit();
				}
			}
			function unchange()
			{
				document.getElementById('r1').value = '';
				document.getElementById('r2').value = '';
                                document.getElementById('room_id1').value = '';
                                document.getElementById('room_id2').value = '';
				document.getElementById('d1').style.display = 'none';
				document.getElementById('d2').style.display = 'none';
				document.getElementById('mod').value = 'select';
				if(document.getElementById('sub'))
					document.getElementById('sub').style.display='none';
                                if(document.getElementById('toz'))
                                        document.getElementById('toz').style.display='none';
			}
			function change()
			{
				document.getElementById('mod').value = 'change';
				document.getElementById('frm1').submit();
			}
		</script>
		<script type="text/javascript">
	    $(function() {
	        //-----------------------------------
	        // انتخاب با کلیک بر روی عکس
	        $("#datepicker6").datepicker({
	            showOn: 'button',
		    dateFormat: 'yy/mm/dd',
	            buttonImage: '../js/styles/images/calendar.png',
	            buttonImageOnly: true
	        });
	    });
    </script>
		<style>
			td{text-align:center;}
		</style>
		<title>
			سامانه رزرواسیون	
		</title>
	</head>
	<body>
                <?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>
		<div align="center">
			<?php
                                echo $outvazeat ;
                        ?>
			<?php echo (($can_change)?'<span id="toz" >جابجایی '.$reserver1.'</span>':''); ?>
			<form id='frm1' method="post">
				<input type='hidden' id='h_id' name='h_id' value='<?php echo $h_id; ?>' /><br/>
				<input type='hidden' id='mod' name='mod' value='select' /><br/>
				<table width='50%'>
					<tr id='d1' style='display:<?php echo (($can_change)?'':'none');?>;'>
						<td>
							شماره رزرو اول : <input type='text' id='r1' name='r1' readonly='readonly' class='inp' value='<?php echo $r1; ?>' />
						</td>
						<td style='display:<?php echo (($can_change)?'':'none');?>;'>
                                                        شماره اتاق اول : <input type='text' class='inp' readonly='readonly' id='room_name1' value='<?php echo $room_name1; ?>' /><input type='hidden' id='room_id1' name='room_id1' readonly='readonly' class='inp' value='<?php echo $room_id1; ?>' />
                                                </td>
						<td style='display:<?php echo (($can_change)?'':'none');?>;'>
							از تاریخ : <input type='text' id='azt1' name='azt1' readonly='readonly' class='inp' value='<?php echo audit_class::hamed_pdate($azt1); ?>' />
						</td>
						<td style='display:<?php echo (($can_change)?'':'none');?>;'>
                	                                تا تاریخ : <input type='text' id='tat1' name='tat1' readonly='readonly' class='inp' value='<?php echo audit_class::hamed_pdate($tat1); ?>' />
        	                                </td>
					</tr>
					<tr id='d2' style='display:<?php echo (($can_change)?'':'none');?>;'>
						<td>
							<input type='text' id='r2' name='r2' readonly='readonly' style='display:;' class='inp' value='<?php echo $r2; ?>' />
							&nbsp;
						</td>
                                                <td style='display:<?php echo (($can_change)?'':'none');?>;'>
                                                        شماره اتاق دوم : <input type='text' class='inp' id='room_name2' readonly='readonly' value='<?php echo $room_name2; ?>' /><input type='hidden' id='room_id2' name='room_id2' readonly='readonly' class='inp' value='<?php echo $room_id2 ?>' />
                                                </td>
                                	        <td style='display:<?php echo (($can_change)?'':'none');?>;'>
                        	                        <input type='text' id='azt2' name='azt2' readonly='readonly' style='display:;' class='inp' value='<?php echo audit_class::hamed_pdate($azt2); ?>' />
							&nbsp;
                	                        </td>
        	                                <td style='display:<?php echo (($can_change)?'':'none');?>;'>
	                                                <input type='text' id='tat2' name='tat2' readonly='readonly' style='display:;' class='inp' value='<?php echo audit_class::hamed_pdate($tat2); ?>' />
                                        	</td>
					</tr>
					<tr>
						<td colspan='4' align='center'>
							تاریخ جابجایی : <input type='text' id='jday' name='jday' readonly='readonly' class='inp' value='<?php echo audit_class::hamed_pdate($jday); ?>' />
						</td>
					</tr>
					<tr id='loading' style='display:none;'>
						<td colspan='4' align='center'>
							<img src="../class/wait.gif" width='30px' alt="در حال بروزرسانی"/>
						</td>
					</tr>
					<tr>
						<td colspan='4' align='center'>
				<?php
					if($can_change)
						echo "<input id='sub' type='button' value='جابجایی' onclick='change();' class='inp'/>";
				?>
							<input type='button' value='جدید' onclick='unchange();' class='inp'/>
						</td>
					</tr>
				</table>
			</form>	
		</div>
		<script language="javascript">
			<?php echo $msg; ?>
		</script>
	</body>
</html>
