<?php
	session_start();
	include("../kernel.php");
        if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        //if(!$se->can_view)
                //die(lang_fa_class::access_deny);
	function hamed_pdate($str)
        {
                $out=jdate('Y/n/j',strtotime($str));
                return $out;
        }
	function  loadHotel($inp=-1)
	{
		$inp = (int)$inp;
		$hotelList=daftar_class::hotelList((int)$_SESSION['daftar_id']);
		$shart = '';
		if($hotelList)
			$shart=' and ( `id`='.implode(" or `id`=",$hotelList).")";
		$out = '<select name="hotel_id" class="inp" style="width:auto;" >';
		mysql_class::ex_sql("select `id`,`name` from `hotel` where `moeen_id` > 0 $shart order by `name` ",$q);
		while($r = mysql_fetch_array($q))
		{
			$sel = (($r['id']==$inp)?'selected="selected"':'');
			$out.="<option $sel  value='".$r['id']."' >".$r['name']."</option>\n";
		}
		$out.='</select>';
		return $out;
	}
	function loadTyp($typ)
	{
		$sel1 =($typ==1)?'selected="selected"':'';
		$sel2 =($typ==2)?'selected="selected"':'';
		$out="<option value='1' $sel1 >تعداد میهمان</option>\n";
		$out.="<option value='2' $sel2 >تعداد اتاق</option>\n";
		return $out;
	}
	function getPic($datay,$us_id)
	{
		if(count($datay)==0)
			$datay = array(0);
		else if(count($datay)>31)
			{
				$datay1 =array();
				$k = 0;
				foreach($datay as $tar=>$val)
				{
					$datay1[$tar] = $val;
					$k++;
					if($k>30)
						break;
				}
			}
		else if(count($datay)<=31)
			$datay1= $datay;
		$datay =array();
		$datax = array();
		foreach($datay1 as $tarikh=>$value)
		{
			$datax[] = $tarikh;
			$datay[] = $value;
		}
		$graph = new Graph(750,300,'auto');
	    	$graph->img->SetMargin(40,40,40,40);
		$graph->img->SetAntiAliasing();
		$graph->SetScale("textlin",0,200);
	    	$graph->SetShadow();
	    	$graph->title->Set(" ");
	    	$p1 = new BarPlot($datay);
	    	$abplot = new AccBarPlot(array($p1));
		$abplot->SetShadow();
		$abplot->value->Show();
	    	$p1->SetColor("blue");
	    	$p1->SetCenter();
		$graph->SetMargin(40,10,40,80);
		$graph->xaxis->SetTickSide(SIDE_BOTTOM);
		$graph->xaxis->SetTickLabels($datax);
		$graph->xaxis->SetLabelAngle(90);
	    	$graph->Add($abplot);
	    	$addr = "chart/$us_id.png";
	    	$graph->Stroke($addr);
		return $addr;
	}
	$hotel_id = (isset($_REQUEST['hotel_id']))?(int)$_REQUEST['hotel_id']:-1;
	$typ = (isset($_REQUEST['typ']))?(int)$_REQUEST['typ']:-1;
	$aztarikh = ((isset($_REQUEST['aztarikh']))?audit_class::hamed_pdateBack($_REQUEST['aztarikh']):date('Y-m-d H:i:s'));
	$tatarikh = ((isset($_REQUEST['tatarikh']))?audit_class::hamed_pdateBack($_REQUEST['tatarikh']):date('Y-m-d H:i:s'));
	$aztarikh = date("Y-m-d 00:00:00",strtotime($aztarikh));
	$tatarikh = date("Y-m-d 23:59:59",strtotime($tatarikh));
	$user_id=(int)$_SESSION['user_id'];
	$out = '';
	$datay = array();
	$addr = '';
	$k = 0;
	if($typ==1)
	{
		$zarfiat_kol = hotel_class::getKolZarfiat($hotel_id );
		$tedad_now = hotel_class::getFullTedad($hotel_id,$aztarikh,$tatarikh);
		$tedad_jam =0;
		$zarfiat_kol_jam = 0;
		$out = '<br/><table cellspacing="10" border="1" style="background-color:#fff;width:50%; font-size: 13px; border-width: 1px; border-collapse: collapse;" ><tr><th>ردیف</th><th>تاریخ</th><th>تعداد میهمانان</th><th>ظرفیت کل</th><th>درصد اشغال</th></tr>';
		foreach($tedad_now as $tarikh=>$tedad)
		{
			$k++;
			$darsad_eshghal = ($zarfiat_kol!=0)?(string)number_format($tedad/$zarfiat_kol,2)*100 .'%':'ظرفیت کل تعریف نشده است';
			$out.="<tr><td>$k</td><td>$tarikh</td><td>$tedad</td><td>$zarfiat_kol</td><td>$darsad_eshghal</td></tr>\n";
			$tedad_jam += $tedad;
			$zarfiat_kol_jam += $zarfiat_kol;
			$datay[$tarikh] = $darsad_eshghal;
		}
		$darsad_eshghal_jam = ($zarfiat_kol_jam!=0)?(string)number_format($tedad_jam/$zarfiat_kol_jam,2)*100 .'%':'ظرفیت کل تعریف نشده است';
		$out .="<tr style='font-weight:bold;' ><td colspan='2' >مجموع</td><td>$tedad_jam</td><td>$zarfiat_kol_jam</td><td>$darsad_eshghal_jam</td></tr>";
		$out .='</table>';
		$addr = getPic($datay,$user_id);
	}
	else if($typ==2)
	{
		$eshghal_jam=0;
		$kol_jam = 0;
		$room_now = hotel_class::getFullRoom($hotel_id,$aztarikh,$tatarikh);
		$out = '<br/><table cellspacing="10" border="1" style="background-color:#fff;width:50%; font-size: 13px; border-width: 1px; border-collapse: collapse;" ><tr><th>ردیف</th><th>تاریخ</th><th>جزئیات</th></tr>';
		foreach($room_now as $tarikh=>$room)
		{
			$eshghal_kol = 0;
			$kol_kol = 0;
			$k++;
			$out.="<tr><td>$k</td><td>$tarikh</td><td>";
			$out .='<table cellspacing="10" border="1" style="background-color:#fff;width:100%; font-size: 13px; border-width: 1px; border-collapse: collapse;" ><tr><th>نوع</th><th>اشغال</th><th>کل</th><th>درصد اشغال</th></tr>';
			foreach($room as $name=>$vaz)
			{
				$darsad_esh = ($vaz['kol']!=0)?(string)number_format($vaz['eshghal']/$vaz['kol'],2)*100 .'%':'نامعلوم';
				$out .="<tr><td>$name</td><td>".$vaz['eshghal']."</td><td>".$vaz['kol']."</td><td>$darsad_esh</td></tr>";
				$eshghal_kol += $vaz['eshghal'];
				$kol_kol += $vaz['kol'];
			}
			$darsad_esh_kol =  ($kol_kol!=0)?(string)number_format($eshghal_kol/$kol_kol,2)*100 .'%':'نامعلوم';
			$datay[$tarikh] = ($kol_kol!=0)?(int)(($eshghal_kol/$kol_kol)*100):0;
			$out .="<tr><td>جمع</td><td>$eshghal_kol</td><td>$kol_kol</td><td>$darsad_esh_kol</td></tr>";
			$out .= "</table></td>";
			$eshghal_jam+=$eshghal_kol;
			$kol_jam+=$kol_kol;
		}
		$darsad_esh_jam = ($kol_jam!=0)?(string)number_format($eshghal_jam/$kol_jam,2)*100 .'%':'ظرفیت کل تعریف نشده است';
		$out .="<tr><td colspan='2' >جمع کل</td><td><table cellspacing='10' border='1' style='background-color:#fff;width:100%; font-size: 13px; border-width: 1px; border-collapse: collapse;'><tr style='font-weight:bold;' ><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>اشغال:$eshghal_jam</td><td>کل:$kol_jam</td><td>درصداشغال:$darsad_esh_jam</td></tr></table></td></tr>";
		$out .='</table>';
		$addr = getPic($datay,$user_id);
	}
		$pic = '';
		if($addr!='')
			$pic ="<img src='$addr' width='700px' style='cursor:pointer;'>"
?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link type="text/css" href="../css/style.css" rel="stylesheet" />

		<link type="text/css" href="../js/styles/jquery-ui-1.8.14.css" rel="stylesheet" />
		<script type="text/javascript" src="../js/jquery/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../js/jquery/jquery.ui.datepicker-cc.all.min.js"></script>
		<script type="text/javascript" src="../js/tavanir.js"></script>
		<script type="text/javascript">
		function send_search()
		{
			document.getElementById('mod').value= 2;
			document.getElementById('frm1').submit();
		}
		function set_value(inp)
		{
			document.getElementById('mablagh').value = document.getElementById(inp).innerHTML;
		}
		</script>
		<script type="text/javascript">
		    $(function() {
			//-----------------------------------
			// انتخاب با کلیک بر روی عکس
			$("#datepicker6").datepicker({
			    showOn: 'button',
			    dateFormat: 'yy/mm/dd',
			    buttonImage: '../js/styles/images/calendar.png',
			    buttonImageOnly: true
			});
		    });
		    $(function() {
			//-----------------------------------
			// انتخاب با کلیک بر روی عکس
			$("#datepicker7").datepicker({
			    showOn: 'button',
			    dateFormat: 'yy/mm/dd',
			    buttonImage: '../js/styles/images/calendar.png',
			    buttonImageOnly: true
			});
		    });
	    	</script>
		<style type="text/css" >
			td{text-align:center;}
		</style>
		<title>
			گزارش تعداد میهمان روز	
		</title>
	</head>
	<body>
                <?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>
		<div align="right" style="padding-right:30px;padding-top:10px;">
			<a href="help.php" target="_blank"><img src="../img/help.png"/></a>
		</div>
		<div align="center">
			<br/>
			<br/>
			<form id='frm1'  method='GET' >
			<table border='1' style='font-size:12px;width:95%;' >
				<tr>
					<th>نام هتل</th>
					<th>نوع</th>
					<th>از تاریخ</th>
					<th>تا تاریخ</th>
					<th>جستجو</th>
				</tr>
				<tr valign="bottom" >
					<td>	
						<?php echo loadHotel($hotel_id); ?>
					</td>
					<td>
						<select class="inp" name="typ" id="typ" >
							<?php echo loadTyp($typ); ?>	
						</select>	
					</td>
					<td>	
         					   <input value="<?php echo ((isset($_REQUEST['aztarikh']))?$_REQUEST['aztarikh']:''); ?>" type="text" name='aztarikh' readonly='readonly' class='inp' style='direction:ltr;' id="datepicker6" />	
					</td>
					<td>
						<input value="<?php echo ((isset($_REQUEST['tatarikh']))?$_REQUEST['tatarikh']:''); ?>" type="text" name='tatarikh' readonly='readonly' class='inp' style='direction:ltr;' id="datepicker7" />
					</td>
					<td>
						<input type='hidden' name='mod' id='mod' value='1' >
						<input type='button' value='جستجو' class='inp' onclick='send_search();' >
					</td>					
				</tr>
			</table>
			</form>
			<?php echo $pic; ?>
			<br/>
			<?php echo $out; ?>
		</div>
	</body>
</html>
