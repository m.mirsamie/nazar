<?php	session_start();
	include_once("../kernel.php");
	if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        if(!$se->can_view)
                die(lang_fa_class::access_deny);
	if(isset($_REQUEST['content']))
	{
		$conf->watcher = $_REQUEST['content'];
	}
	$res = new reserve_class;
	$res->loadWatcher();
	$hel = $res->watcherKeys;
	$table = '<table style="width:70%;font-family:tahoma;border:solid 1px;margin-top:10px;" >';
	$table .="<tr><th colspan='2' >راهنما</th></tr>\n";
	$table .="<tr><th>کلید</th><th>توضیحات</th></tr>\n";
	$table .="<tr><td>table</td><td>جدول اصلی هتل و خدمات</td></tr>\n";
	foreach($hel as $key=>$help)
	{
		$table .="<tr><td>".$key."</td><td>".$help['help']."</td></tr>\n";
	}
	$table.='</table>';
	$matn = $conf->watcher;
	$box = "<textarea  style=\"overflow-y: scroll;overflow-x: scroll;\" name=\"content\" id=\"content\"  >$matn</textarea>";
	$ckscript ='<script type="text/javascript">
						         CKEDITOR.replace( "content" );
	</script>';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->
		<script src="../ckeditor/ckeditor.js" type="text/javascript"></script>
                <script src="../ckeditor/adapters/jquery.js" type="text/javascript"></script>
                <link href="../ckeditor/contents.css" rel="stylesheet" type="text/css" />
		<link type="text/css" href="../css/style.css" rel="stylesheet" />
		<!-- JavaScript Includes -->
		<!-- <script type="text/javascript" src="../js/tavanir.js"></script> -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>
مدیریت واچر
		</title>
		<script language="javascript" >	
			function save_frm()
			{
				if(confirm('آیا اطلاعات دخیره شود؟'))
					document.getElementById('frm1').submit();
			}
			function cancel_frm()
			{
				window.close();	
			}
		</script>
		<style>
		td,th {text-align: center;border:solid 1px;padding:5px;}
		</style>
		<script>
		function st()
		{
		week= new Array("يكشنبه","دوشنبه","سه شنبه","چهارشنبه","پنج شنبه","جمعه","شنبه")
		months = new Array("فروردين","ارديبهشت","خرداد","تير","مرداد","شهريور","مهر","آبان","آذر","دي","بهمن","اسفند");
		a = new Date();
		d= a.getDay();
		day= a.getDate();
		var h=a.getHours();
      		var m=a.getMinutes();
  		var s=a.getSeconds();
		month = a.getMonth()+1;
		year= a.getYear();
		year = (year== 0)?2000:year;
		(year<1000)? (year += 1900):true;
		year -= ( (month < 3) || ((month == 3) && (day < 21)) )? 622:621;
		switch (month) 
		{
			case 1: (day<21)? (month=10, day+=10):(month=11, day-=20); break;
			case 2: (day<20)? (month=11, day+=11):(month=12, day-=19); break;
			case 3: (day<21)? (month=12, day+=9):(month=1, day-=20); break;
			case 4: (day<21)? (month=1, day+=11):(month=2, day-=20); break;
			case 5:
			case 6: (day<22)? (month-=3, day+=10):(month-=2, day-=21); break;
			case 7:
			case 8:
			case 9: (day<23)? (month-=3, day+=9):(month-=2, day-=22); break;
			case 10:(day<23)? (month=7, day+=8):(month=8, day-=22); break;
			case 11:
			case 12:(day<22)? (month-=3, day+=9):(month-=2, day-=21); break;
			default: break;
		}
		//document.write(" "+week[d]+" "+day+" "+months[month-1]+" "+ year+" "+h+":"+m+":"+s);
			var total=" "+week[d]+" "+day+" "+months[month-1]+" "+ year+" "+h+":"+m+":"+s;
			    document.getElementById("tim").innerHTML=total;
   			    setTimeout('st()',500);
		}
		</script>
	</head>
	<body dir="rtl" onload='st()'>
		<center>
		<span id='tim' >test2
		</span>
		</center>
		<div style="width:99%;border: solid 1px;padding:3px;margin:5px;" align="center">
			<form id='frm1' method="POST" >
			<?php 
				echo $box.'<br />';
				echo $ckscript;
				
			?>
			
			<input type="button" class="inp" value="ذخیره سازی"  onclick="save_frm();" >
			<input type="button" class="inp" value="انصراف"  onclick="cancel_frm();" >
			</form>
			<?php
				echo $table;
			?>
		</div>
	</body>

</html>
