<?php
	session_start();
	include("../kernel.php");
        if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        if(!$se->can_view)
                die(lang_fa_class::access_deny);
	function loadNesbat()
	{
		$tmp = statics_class::loadByKey('نسبت');
		$out['سرگروه'] = '-1';
		for($i=0;$i<count($tmp);$i++)
			$out[$tmp[$i]->fvalue]=$tmp[$i]->id;
		return $out;
	}
	function loadMellait()
	{
		$tmp = statics_class::loadByKey('ملیت');
		for($i=0;$i<count($tmp);$i++)
			$out[$tmp[$i]->fvalue]=$tmp[$i]->id;
		return $out;
	}
	$msg = '';
	$out = '';
	$scr = '';
	$vaziat = 0;
	$room_id = -1;
	$reserve_id = -1;
	$isAdmin = $se->detailAuth('all');
	$output = '<br/><table border="1" cellpadding="0" cellspacing="0" width="95%" style="font-size:12px;border-style:solid;border-width:1px;border-color:Black;margin:10px;cell-padding:50px;" ><tr class="showgrid_header" ><th>پذیرش</th><th>شماره رزرو</th><th>هتل</th><th>نام</th><th>شماره اتاق</th><th>تعداد نفرات</th><th>قیمت هتل</th><th>جمع کل</th><th>تاریخ ورود</th><th>تاریخ خروج</th></tr>';
	$changed = FALSE;
	$room_loded = FALSE;
	$msg = '';
	$tarikh_true = TRUE;
	if(!isset($_REQUEST['reserve_id']) && isset($_REQUEST['room_id']))
	{
		$tarikh = ((isset($_REQUEST['tarikh']))?audit_class::hamed_pdateBack($_REQUEST['tarikh']):'0000-00-00 00:00:00');
		$r_tmp = new room_class((int)$_REQUEST['room_id']);
		$room_id = (int)$_REQUEST['room_id'];
		$vaziat = $r_tmp->vaziat;
		$res = $r_tmp->getAnyReserve($tarikh);
		if(isset($_REQUEST['tarikh']) )
		if(strtotime(date("Y-m-d H:i:s")) > strtotime($tarikh))
		{
			$tarikh_true = FALSE;
			$msg = 'تاریخ درست وارد نشده است';
		}
			
/*
		$_REQUEST['vaziat'] = $r_tmp->vaziat;
		$reserve_id = $reserve_id[0]['reserve_id'];
		$_REQUEST['reserve_id'] = $reserve_id;
*/
                if(isset($_REQUEST['vaziat']) && $res==null && $tarikh_true)
                {
                        $vaziat = (int)$_REQUEST['vaziat'];
                        $room_id = (int)$_REQUEST['room_id'];
                        mysql_class::ex_sqlx("update `room` set `vaziat` = $vaziat,`end_fix_date`='$tarikh' where `id` = $room_id");
                        $scr = "<script language='javascript'> window.parent.location = window.parent.location; </script>";
                }
		if($res!=null)
		{
			$_REQUEST['vaziat'] = $r_tmp->vaziat;
			$reserve_id = $res[0]['reserve_id'];
			$_REQUEST['reserve_id'] = $reserve_id;
			$room_id = (int)$_REQUEST['room_id'];
		}
		$room_loded = TRUE;
		$reserve_rooms = '<option selected="selected" value="'.$room_id.'">'.$r_tmp->name.'</option>'."\n";
	}
	else if(isset($_REQUEST['reserve_id']))
		$changed = TRUE;
	if($se->detailAuth('tasisat'))
		die('<script>window.location="tasisat.php?room_id='.$room_id.'"</script>');
	if(isset($_REQUEST['reserve_id']) && (int)$_REQUEST['reserve_id']>0)
	{
		$reserve_id = (int)$_REQUEST['reserve_id'];
		$vaziat = 0;
		$room_id = -1;
		if(isset($_REQUEST['vaziat']))
		{
			$vaziat = (int)$_REQUEST['vaziat'];
			$room_id = (int)$_REQUEST['room_id'];
			mysql_class::ex_sqlx("update `room` set `vaziat` = $vaziat where `id` = $room_id");
			if($changed)
				$scr = "<script language='javascript'> window.parent.location = window.parent.location; </script>";
		}
		$reserve_rooms = '';
		$reserve_room_det = new room_det_class;
		$reserve_room_det = $reserve_room_det->loadByReserve($reserve_id);
		$reserve_room_det = $reserve_room_det[0];
		for($ind = 0;$ind < count($reserve_room_det);$ind++)
		{
			$r_tmp = new room_class($reserve_room_det[$ind]->room_id);
			$reserve_rooms .= '<option '.(($reserve_room_det[$ind]->room_id==$room_id)?'selected="selected"':'').' value="'.$reserve_room_det[$ind]->room_id.'">'.$r_tmp->name.'</option>'."\n";
		}
		$styl = 'class="showgrid_row_odd"';
		$horel_reserve = new hotel_reserve_class;
		$horel_reserve->loadByReserve($reserve_id);
		$ajans = new ajans_class($horel_reserve->ajans_id);
		//--------------------------
		$reserve_id_code =dechex($reserve_id+10000);
		$khorooj = '';
		/*
		if(reserve_class::isKhorooj($reserve_id))
			$khorooj = "<sapn style='color:green'>خارج شده</span>";
		else if(reserve_class::isPaziresh($reserve_id))
			$khorooj = "<a style='color:red' target='_blank' href='paziresh.php?reserve_id=$reserve_id_code&kh=1' >خروج</a>";
		*/
		//$paziresh ="<td><a target='_blank' href='paziresh.php?reserve_id=$reserve_id_code&kh=0' >پذیرش</a>&nbsp;$khorooj</td>";
		$paziresh ='<td></td>';
		//--------------------------
		if(($_SESSION['daftar_id']==$ajans->daftar_id || $isAdmin) && !$se->detailAuth('super'))
		{
			$room = room_det_class::loadDetByReserve_id($reserve_id );
			$rooms = '';
			for($j=0;$j<count($room['rooms']);$j++)
			{
				$tmp_room = new room_class($room['rooms'][$j]['room_id']);
				$rooms.=$tmp_room->name.(($j<count($room['rooms'])-1)?' , ':'');
			}
			$name = room_det_class::loadNamesByReserve_id($reserve_id );
			$khadamat = room_det_class::loadKhadamatByReserve_id($reserve_id );
			$output .="<tr $styl >$paziresh<td>$reserve_id</td>";
			$output .="<td>".$room['rooms'][0]['hotel']."</td><td>".$name[0]."</td><td>$rooms</td><td>".$room['rooms'][0]['nafar']."</td><td>".monize($horel_reserve->m_hotel)."</td>";
			$output .="<td>".monize($horel_reserve->m_belit+$horel_reserve->m_hotel)."</td>";
			$output .="<td>".audit_class::hamed_pdate($room['rooms'][0]['aztarikh'])."</td>";
			$output .="<td>".audit_class::hamed_pdate($room['rooms'][0]['tatarikh'])."</td></tr>";
			if($horel_reserve->extra_toz!='')
				$output .="<tr $styl ><td>توضیحات : </td><td colspan='10'>".$horel_reserve->extra_toz."</td></tr>";
			//-----------------------------------------
			$grid = new jshowGrid_new("mehman","grid1");
			$grid->width = '99%';
			$grid->index_width = '20px';
			$grid->showAddDefault = FALSE;
			$grid->whereClause = "`reserve_id`='$reserve_id' ";
			for($i=0;$i<count($grid->columnHeaders);$i++)
				$grid->columnHeaders[$i] = null;
			$grid->columnHeaders[2] = 'نام';
			$grid->columnHeaders[3] = 'نام  خانوادگی';
			$grid->columnHeaders[9] = 'ملیت';
			$grid->columnLists[9]=loadMellait();
			$grid->columnHeaders[16] = 'نسبت';
			$grid->columnLists[16]=loadNesbat();
			//$grid->sortEnabled = TRUE;
			$b = FALSE;
			$grid->canEdit = $b;
			$grid->canAdd = $b;
			$grid->canDelete = $b;
			$grid->intial();
		   	$grid->executeQuery();
			$out = $grid->getGrid();
		}
		else
			$output='شما به این رزرو دسترسی ندارید';
	}
	$output .='</table><br/><h3>اطلاعات پذیرش</h3>'.$out;
	$dis = ($vaziat==4 or $vaziat==5) ? '':'none';
	$tarikh_view = ($vaziat==4 or $vaziat==5) ? audit_class::hamed_pdate($r_tmp->end_fix_date):'';
?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link type="text/css" href="../css/style.css" rel="stylesheet" />

		<link type="text/css" href="../js/styles/jquery-ui-1.8.14.css" rel="stylesheet" />
		<script type="text/javascript" src="../js/jquery/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../js/jquery/jquery.ui.datepicker-cc.all.min.js"></script>
		<script type="text/javascript" src="../js/tavanir.js"></script>
		<script type="text/javascript">
		function send_search()
		{
			document.getElementById('mod').value= 2;
			document.getElementById('frm1').submit();
		}
		function sendback()
		{
			if($("#tarikh").is(":visible"))
			{
				if($("#tarikh").val()!='')
					document.getElementById('frm1').submit();
				else
					alert('تاریخ را وارد کنید');
			}
			else
				document.getElementById('frm1').submit();
		}
		function statusCH()
		{
			var vaziat = $("#vaziat").val();
			if(vaziat=="4" || vaziat=="5")
				$("#div_tarikh").show('slow');
			else
				$("#div_tarikh").hide('slow');
		}
		$(function() {
	        //-----------------------------------
	        // انتخاب با کلیک بر روی عکس
	        $("#tarikh").datepicker({
	            showOn: 'button',
		    dateFormat: 'yy/mm/dd',
	            buttonImage: '../js/styles/images/calendar.png',
	            buttonImageOnly: true
	        });
	    });
		</script>
		<style>
			td{text-align:center;}
		</style>
		<title>
			سامانه رزرواسیون	
		</title>
	</head>
	<body>
                <?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>
		<div align="center">
			
			<?php echo $output.' '.$msg; ?>
		</div>
		<?php echo $scr; ?>
	</body>
</html>
