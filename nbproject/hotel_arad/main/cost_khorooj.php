<?php
	session_start();
        include_once("../kernel.php");
        if(!isset($_SESSION['user_id']))
                die(lang_fa_class::access_deny);
        $se = security_class::auth((int)$_SESSION['user_id']);
        if(!$se->can_view)
                die(lang_fa_class::access_deny);
	function hamed_pdate($str)
        {
                $out=jdate('Y/n/j',strtotime($str));
                return $out;
        }
	function hamed_pdateBack($inp)
	{
		$out = FALSE;
                $tmp = explode("/",$inp);
                if (count($tmp)==3)
                {
                        $y=(int)$tmp[2];
                        $m=(int)$tmp[1];
                        $d=(int)$tmp[0];
                        if ($d>$y)
                        {
                                $tmp=$y;
                                $y=$d;
                                $d=$tmp;
                        }
                        if ($y<1000)
                        {
                                $y=$y+1300;
                        }
                        $inp="$y/$m/$d";
                        $out = audit_class::hamed_jalalitomiladi(audit_class::perToEn($inp));
                }

                return $out." 12:00:00";
	}
	function loadKalaTarkibi()
	{
		$out ='<select class="inp" name="kala_cost" id="kala_cost" >';
		mysql_class::ex_sql("select `id`,`name` from `cost_kala` where `is_personal`=1 order by `name`",$q);
		while($r=mysql_fetch_array($q))
			$out .="<option value='".$r['id']."' >".$r['name']."</option>\n";
		$out .='</select>';
		return $out;
	}
	function loadAnbar()
	{
		$out = '<select class="inp" name="anbar_id" id="anbar_id" >';
		mysql_class::ex_sql('select `name`,`id` from `anbar` where `en`<>2 order by `name`',$q);
		while($r = mysql_fetch_array($q))
			$out.= "<option  value='".$r['id']."' >".$r['name']."</option>\n";
		$out .='</select>';
		return($out);
	}
	function loadUsers()
	{
		$out = '<select class="inp" name="gUser_id" id="gUser_id" >';
		mysql_class::ex_sql('select `lname`,`fname`,`id` from `user` where `user`<>\'mehrdad\' order by `lname`,`fname`',$q);
		while($r = mysql_fetch_array($q))
			$out.= "<option  value='".$r['id']."' >".$r['lname'].' '.$r['fname']."</option>\n";
		$out .='</select>';
		return($out);
	}
	function loadKhad($hotel_id)
	{
		$out = '';
		mysql_class::ex_sql("select `id`,`name` from `khadamat` where `en`=1 and `typ`=0 and `hotel_id`=$hotel_id",$q);
		while($r = mysql_fetch_array($q))
			$out.="<option value='".$r['id']."' >".$r['name']."</option>\n";
		return $out;
	}
	$out = '';
	$tarikh =((isset($_REQUEST['tarikh']))?audit_class::hamed_pdateBack($_REQUEST['tarikh']):date("Y-m-d"));
	$tmp = explode(' ',$tarikh);
	$tarikh = $tmp[0];
	if(isset($_REQUEST['hotel_id']) && $_REQUEST['hotel_id']!='')
	{
		$hotel_id = $_REQUEST['hotel_id'];
		$hotel = new hotel_class($hotel_id);
		$hotel_kh = new hotel_class($hotel_id);
		$out = '<table>';
		if(($se->detailAuth('all') || $se->detailAuth('anbar_dari')) )
		{
			$disable = '';
			$pm = '';
			if($hotel_kh->ghaza_moeen_id<0)
			{
				$disable = 'disabled="disabled"';
				$pm = '<span style="color:red" >حساب معین هزینه غذا برای هتل ثبت نشده است</span>';
			}
			$out .="<tr class='showgrid_row_even' ><td style='text-align:left;'>خروج از انبار : </td><td>".loadAnbar()."</td><td>تحویل گیرنده:</td><td>".loadUsers()."</td><td>نعداد:<input name='sum' id='sum' class='inp' style='width:30px;' ></td><td>".loadKalaTarkibi()."<input type='button' value='چاپ رسید خروج از انبار' class='inp' $disable onclick='send_info();'>$pm</td></tr>";
	
		}
		$out .="</table>";
	}


	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Style Includes -->

		<link type="text/css" href="../css/style.css" rel="stylesheet" />

		<!-- JavaScript Includes -->
		<link type="text/css" href="../js/styles/jquery-ui-1.8.14.css" rel="stylesheet" />
		<script type="text/javascript" src="../js/jquery/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="../js/jquery/jquery.ui.datepicker-cc.all.min.js"></script>
		<script type="text/javascript" src="../js/tavanir.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>
		خدمات هتل		
		</title>
		<script type="text/javascript">
		function sbtFrm()
		{
			document.getElementById('frm1').submit();
		}
		function getPrint()
		{
			document.getElementById('div_main').style.width = '18cm';
			window.print();
			document.getElementById('div_main').style.width = 'auto';
		}
		function send_info()
		{
			if(parseInt(document.getElementById('sum').value,10)>0)
			{
				if(confirm('آیا کالا با جزئیات از انبار خارج شود؟'))
						document.getElementById('frm1').submit();
			}
			else
				alert('تعداد کالای خروجی را وارد کنید');
		}
		</script>
		<script type="text/javascript">
		    $(function() {
			//-----------------------------------
			// انتخاب با کلیک بر روی عکس
			$("#tarikh").datepicker({
			    showOn: 'button',
			    dateFormat: 'yy/mm/dd',
			    buttonImage: '../js/styles/images/calendar.png',
			    buttonImageOnly: true
			});
		    });
	    	</script>
	</head>
	<body>
                <?php echo security_class::blockIfBlocked($se,lang_fa_class::block); ?>
		<form method="POST" name="frmtedad" id ="frmtedad">
        	<input name="txttedad" id="txttedad" type="hidden" value="1"/>
	        </form>

		<div align="center" id="div_main" >
			<br/>
			<form id="frm1" method="GET" action="cost_anbar.php">
				<table id='combo_table' >
					</tr>
						<td>
							<label> <?php echo (isset($hotel))?$hotel->name:''; ?> </label>
						</td>
						<td>
							<select name="khadamat_id" id="khadamat_id" class="inp">
								<?php echo loadKhad($hotel_id); ?>
							</select>
						</td>
						<td>
							<label> تاریخ</label>
						</td>
						<td>
							<input class="inp" readonly="readonly" type="text" name="tarikh" id="tarikh" value="<?php echo audit_class::hamed_pdate($tarikh); ?>"  >
							<input type="hidden" name="tarikh1" id="tarikh1" value="<?php echo audit_class::hamed_pdate($tarikh); ?>"  >
				<input type="hidden" name="hotel_id" id="hotel_id" value="<?php echo (isset($_REQUEST['hotel_id']))?$_REQUEST['hotel_id']:''; ?>"  >
						</td>
					</tr>
				</table>		
				<br/>
				<?php echo $out;  ?>
				<br/>
			</form>
		</div>
		
	</body>
</html>
